from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms import ModelForm, fields

from main.models import SystemLanguage, StateLink, Project


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ('date_hosting', 'date_domain')
    
    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


class EditUserForm(forms.Form):
    username = forms.CharField(label='Ник', max_length=255, required=True)
    first_name = forms.CharField(label='Имя', max_length=255, required=True)
    last_name = forms.CharField(label='Фамилия', max_length=255, required=False)
    language = forms.ChoiceField(required=True, label='Язык')
    is_blocked = forms.BooleanField(label='Заблокирован', required=False)

    def __init__(self, *args, **kwargs):
        curr_language = kwargs.pop('curr_language')
        super(EditUserForm, self).__init__(*args, **kwargs)
        languages = SystemLanguage.objects.extra(select={'is_top': "language_code='{}'".format(curr_language)})
        languages = languages.extra(order_by=['-is_top'])
        self.fields['language'].choices = list([(choice, choice) for choice in languages])
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


class AddLanguageForm(ModelForm):
    class Meta:
        model = SystemLanguage
        fields = 'language_code',

    def __init__(self, *args, **kwargs):
        super(AddLanguageForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


class IndexLoginForm(AuthenticationForm):
    username = forms.CharField(label='Логин', max_length=100, required=True)
    password = forms.CharField(widget=forms.PasswordInput(), required=True, max_length=100, label='Пароль')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(IndexLoginForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
        self.fields['username'].widget.attrs['style'] = 'margin-bottom: 10px;'


class StateLinkForm(forms.ModelForm):
    class Meta:
        model = StateLink
        fields = (
            'state',
            'tag',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def clean(self):
        form_data = self.cleaned_data
        tag = form_data.get('tag', None)
        state = form_data.get('state', None)

        if StateLink.objects.filter(tag=tag, state=state).exclude(pk=self.instance.pk).first():
            self._errors['tag'] = ['Такой тег для выбраных параметров ссылки уже существует.']
        return form_data
