from django.contrib.auth.decorators import login_required
from django.urls import path
from django.contrib.auth import views as auth_views
from main.views import PanelLoginView
from panel.modules import load_modules_panel_url
from panel.views import *


app_name = 'panel'

urlpatterns = [
    path('', sender, name='sender'),
    path('broadcast', broadcast, name='broadcast'),
    path('status', status, name='status'),
    path('create', ProjectFormView.as_view(),  name='create'),
    path('project_list', ShowProject.as_view(), name='project_list'),
    path('users', login_required(UsersModeratorView.as_view()), name='users'),
    path('user_history/<int:pk>/', login_required(user_history), name='user_history'),
    path('search_by_users', login_required(search_by_users), name='search_by_users'),
    path('user_edit/<int:pk>/', login_required(EditFormUserView.as_view()), name='edit_user'),
    path('translations', login_required(RowsView.as_view()), name='rows'),
    path('translation_edit', translation_edit, name='translation_edit'),
    path('language_add', login_required(AddLanguageView.as_view()), name='language_add'),
    path('language_delete', language_delete, name='language_delete'),
    path('statistic', get_statistic, name='statistic'),
    path('get_statistic_90_days', get_statistic_90_days, name='get_statistic_90_days'),
    path('get_statistic_365_days', get_statistic_365_days, name='get_statistic_365_days'),
    path('arriving_audience', arriving_audience, name='arriving_audience'),
    path('arriving_audience_90_days', arriving_audience_90_days, name='arriving_audience_90_days'),
    path('arriving_audience_365_days', arriving_audience_365_days, name='arriving_audience_365_days'),
    path('login', PanelLoginView.as_view(), name='login'),
    path('get_all_broadcast_messages', get_all_broadcast_messages, name='get_all_broadcast_messages'),
    path('edit_broadcast', edit_broadcast, name='edit_broadcast'),
    path('remove_broadcast', remove_broadcast, name='remove_broadcast'),
    path('artifact_archive', artifact_archive, name='artifact_archive'),
    path('current_statistic', current_statistic, name='current_statistic'),
    path('click_statistic', click_statistic, name='click_statistic'),
    path('artifact_statistic', artifact_statistic, name='artifact_statistic'),
    path('artifact_users', artifact_users, name='artifact_users'),
    path('artifact_delete/<int:pk>/', ArtifactDeleteView.as_view(), name='artifact_delete'),
    path('artifacts_delete', artifacts_delete, name='artifacts_delete'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('state_links', login_required(StateLinkListView.as_view()), name='state_links'),
    path('create_state_link', login_required(StateLinkCreateView.as_view()), name='create_state_link'),
    path('update_state_link/<int:pk>', login_required(StateLinkUpdateView.as_view()), name='update_state_link'),
    path('remove_state_link/<int:pk>', remove_state_link, name='remove_state_link'),
]


urlpatterns += load_modules_panel_url()
