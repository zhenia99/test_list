from django.conf import settings


def installed_modules(request):
    return {'INSTALLED_MODULES': settings.INSTALLED_MODULES}
