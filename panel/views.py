import re
from datetime import datetime, timedelta

import pygal
from django.contrib.auth.decorators import login_required
from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup

from main.bot.translation_validation import MyHTMLParser
from main.models import User, StateLink, STATES, ScheduledBroadCastSendTask, ScheduledBroadCastInlineButton
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.contrib import messages

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, FormView, CreateView, UpdateView, DeleteView

from django.db.models import Q

from django_scaffold.settings import BOT_TOKEN, STORAGE_CHAT
from main.logger_settings import logger
from main.models import TokenLanguageString, LanguageString, SystemLanguage, Click, Artifact, StatisticUser, \
    BroadCastSendTask, ScheduledBroadCastSendTask, Project
from main.translations import add_new_language_default
from panel.forms import EditUserForm, AddLanguageForm, StateLinkForm, ProjectForm

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient, SenderException
    from main.models import Message, File
else:
    from main.bot.sender_client import SenderClient, SenderException

SENDER_BOT = SenderClient(BOT_TOKEN)

class ShowProject(ListView):
    template_name = 'panel/project_list.html'
    paginate_by = 50
    context_object_name = 'projects'
    model = Project

    def get_queryset(self):
        print(Project.objects.all())
        return Project.objects.all()


class ProjectFormView(CreateView):
    template_name = 'panel/create.html'
    form_class = ProjectForm
    model = Project

    def get_success_url(self):
        return reverse('panel:project_list')


@login_required
def sender(request):
    user_id = request.GET.get('user_id', None)
    languages = SystemLanguage.objects.all()
    context = dict()
    if user_id:
        context['user_id'] = user_id
    context['languages'] = languages
    context['states'] = STATES
    return render(request, 'panel/sender.html', context)


@csrf_exempt
@login_required
def broadcast(request):
    message = request.POST.get('message', None)
    file_to_send = request.FILES.get('file_to_send', None)
    inline_buttons = request.POST.get('inline_buttons')
    send_date = request.POST.get('send_date', None)
    user_list_for_scheduling = []
    if not inline_buttons:
        inline_buttons = []
    else:
        inline_buttons = eval(inline_buttons)
    defined_targets = request.POST.get('defined_targets', None)
    targets = []
    send_by_language = []
    if not message and not file_to_send:
        return HttpResponse('Невозможно отправить пустое сообщение без файла', status=501)
    html_parser = MyHTMLParser()
    correct_html, error_msg = html_parser.feed(message)
    if not correct_html:
        return HttpResponse('Ошибка в тексте сообщения связаная с html: {}'.format(error_msg), status=501)
    if not defined_targets:
        for language in SystemLanguage.objects.iterator():
            if request.POST.get(language.language_code, None) == 'yes':
                send_by_language.append(language)
        if send_by_language:
            for language in send_by_language:
                for user in User.objects.filter(language=language).exclude(user_id_int=None).exclude(is_blocked=True):
                    if user.user_id_int not in targets:
                        targets.append(user.user_id_int)
                        if send_date:
                            user_list_for_scheduling.append(user)
        else:
            for user in User.objects.exclude(is_blocked=True):
                if user.user_id_s:
                    targets.append(user.user_id_s)
                    if send_date:
                        user_list_for_scheduling.append(user)
    else:
        for user_id_str in filter(None, map(str.strip, re.split('[,\s]', defined_targets))):
            if user_id_str.isdigit():
                if User.objects.filter(user_id_s=user_id_str).count():
                    if User.objects.filter(user_id_s=user_id_str).get().is_blocked != True:
                        targets.append(int(user_id_str))
                    if send_date:
                        user_list_for_scheduling.append(User.objects.get(user_id_s=user_id_str))
                else:
                    logger.error('User with id {} not found (/sendemmesage processor)'.format(user_id_str))
                    return HttpResponse('Пользователь с id {} не найден'.format(user_id_str), status=501)
            else:
                logger.error('Incorrect user id {} (/sendemmesage processor)'.format(user_id_str))
                return HttpResponse('Неправильный id пользователя {}'.format(user_id_str), status=501)

    scheduler_broadcast = None
    if send_date:
        scheduler_broadcast = ScheduledBroadCastSendTask.objects.create(message=message,
                                                                        send_date=send_date)
        scheduler_broadcast.targets.add(*user_list_for_scheduling)

    keyboard = None
    if inline_buttons:
        max_width = max([thing['pos'] for thing in inline_buttons])
        keyboard = InlineKeyboardMarkup(row_width=max_width)
        row_index = 0
        buttons_list = []
        for each in inline_buttons:
            if each['row'] > row_index:
                keyboard.add(*buttons_list)
                buttons_list = []
                row_index = each['row']
            if each['is_state'] == 'True':
                inline_button = InlineKeyboardButton(each['text'], callback_data=f'to_state;{each["state"]}')
            else:
                inline_button = InlineKeyboardButton(each['text'], url=each['url'])
            buttons_list.append(inline_button)
            if scheduler_broadcast:
                ScheduledBroadCastInlineButton.objects.create(scheduled_task=scheduler_broadcast,
                                                              text=each['text'],
                                                              url=each['url'],
                                                              state=each['state'],
                                                              to_state_enabled=each['is_state'],
                                                              row=each['row'],
                                                              column=each['pos'])
        # add last row
        if len(buttons_list):
            keyboard.add(*buttons_list)

    if len(targets):
        if file_to_send is not None:
            bot = SENDER_BOT.get_bot()
            if file_to_send.content_type.find('audio/') != -1:
                message_media = bot.send_audio(STORAGE_CHAT, file_to_send.file, title=str(file_to_send))
                if scheduler_broadcast:
                    scheduler_broadcast.file_type = 'audio'
                    scheduler_broadcast.file_to_send_id = message_media.audio.file_id
                    scheduler_broadcast.save(update_fields=['file_type', 'file_to_send_id'])
                    return HttpResponse('!', status=200)

                response = SENDER_BOT.send_audio(
                    targets,
                    message_media.audio.file_id,
                    message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
            elif file_to_send.content_type.find('image/') != -1:
                message_media = bot.send_photo(STORAGE_CHAT, file_to_send.file)
                if scheduler_broadcast:
                    scheduler_broadcast.file_type = 'image'
                    scheduler_broadcast.file_to_send_id = message_media.photo[-1].file_id
                    scheduler_broadcast.save(update_fields=['file_type', 'file_to_send_id'])
                    return HttpResponse('!', status=200)

                response = SENDER_BOT.send_photo(
                    targets,
                    message_media.photo[-1].file_id,
                    message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
            elif file_to_send.content_type.find('video/') != -1:
                message_media = bot.send_video(STORAGE_CHAT, file_to_send.file)
                if scheduler_broadcast:
                    scheduler_broadcast.file_type = 'video'
                    scheduler_broadcast.file_to_send_id = message_media.video.file_id
                    scheduler_broadcast.save(update_fields=['file_type', 'file_to_send_id'])
                    return HttpResponse('!', status=200)

                response = SENDER_BOT.send_video(
                    targets,
                    message_media.video.file_id,
                    message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
            else:
                message_media = bot.send_document(STORAGE_CHAT, file_to_send.file)
                if scheduler_broadcast:
                    scheduler_broadcast.file_type = 'document'
                    scheduler_broadcast.file_to_send_id = message_media.document.file_id
                    scheduler_broadcast.save(update_fields=['file_type', 'file_to_send_id'])
                    return HttpResponse('!', status=200)

                response = SENDER_BOT.send_document(
                    targets,
                    message_media.document.file_id,
                    message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
        else:
            if scheduler_broadcast:
                return HttpResponse('!', status=200)

            response = SENDER_BOT.send_message(
                targets,
                message,
                priority=0,
                broadcast=True,
                reply_markup=keyboard
            )
        broadcast_task = BroadCastSendTask()
        if message:
            broadcast_task.message = message
        broadcast_task.task_id = response.text
        broadcast_task.save()
    return HttpResponse('!', status=200)


@csrf_exempt
@login_required
def status(request):
    try:
        return HttpResponse(SENDER_BOT.get_status())
    except SenderException:
        logger.exception('Sender exception occurred while receiving sender status')
        return JsonResponse({0: '-1/0'})
        # return jsonify(0, '-1/0')


class UsersModeratorView(ListView):
    template_name = 'panel/users.html'
    paginate_by = 50
    context_object_name = 'users'
    model = User

    def get_queryset(self):
        order_by = self.request.GET.get('order_by', None)
        if order_by:
            return User.objects.exclude(is_superuser=True).order_by(order_by)
        return User.objects.exclude(is_superuser=True)


@csrf_exempt
def search_by_users(request):
    query = request.GET.get('q', None)
    if query == 'all_users':
        searching_result = User.objects.exclude(is_superuser=True)
    else:
        searching_result = User.objects.exclude(is_superuser=True).filter(Q(username__icontains=query) |
                                                                          Q(user_id_s__icontains=query) |
                                                                          Q(first_name__icontains=query) |
                                                                          Q(username__icontains=query))
    data = {'users': []}
    fields = [
        'id',
        'user_id_s',
        'username',
        'username_tg',
        'first_name',
        'last_name',
        'language',
        'is_blocked',
    ]

    for result in searching_result:
        data['users'].append(model_to_dict(result, fields))
    return JsonResponse(data)


class EditFormUserView(FormView):
    template_name = 'panel/user_edit.html'
    form_class = EditUserForm
    curr_language = None

    def form_valid(self, form):
        user_pk = self.kwargs['pk']
        user = User.objects.filter(pk=user_pk).first()
        user.username_tg = form.cleaned_data.get('username', None)
        user.first_name = form.cleaned_data.get('first_name', None)
        user.last_name = form.cleaned_data.get('last_name', None)
        user.language = form.cleaned_data.get('language', None)
        user.is_blocked = form.cleaned_data.get('is_blocked', False)
        user.save()
        self.success_url = reverse('panel:edit_user', args=[user_pk])
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_pk = self.kwargs['pk']
        user = User.objects.filter(pk=user_pk).first()
        if user:
            form = context['form']
            form.fields['username'].widget.attrs['value'] = user.username_tg if user.username_tg \
                else user.username
            form.fields['first_name'].widget.attrs['value'] = user.first_name
            form.fields['last_name'].widget.attrs['value'] = user.last_name if user.last_name else ''
            form.fields['language'].widget.attrs['value'] = user.language
            if user.is_blocked:
                form.fields['is_blocked'].widget.attrs['checked'] = None
        return context

    def get(self, request, *args, **kwargs):
        user_pk = self.kwargs['pk']
        user = User.objects.filter(pk=user_pk).first()
        if not user:
            return HttpResponse('User does not exist', status=404)
        return super().get(request, args, kwargs)

    def get_form_kwargs(self):
        kwargs = super(EditFormUserView, self).get_form_kwargs()
        user_pk = self.kwargs['pk']
        user = User.objects.filter(pk=user_pk).first()
        if user:
            kwargs.update({'curr_language': user.language})
        return kwargs

    def get_success_url(self):
        return reverse('panel:users')


class RowsView(ListView):
    model = SystemLanguage
    template_name = 'panel/localization.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        codes = SystemLanguage.objects.all().order_by('language_code')
        context['codes'] = codes
        tokens = TokenLanguageString.objects.all()
        result_dict = {}
        for token in tokens:
            result_dict[token.name] = []
            for lang in LanguageString.objects.filter(token=token).order_by('language_code__language_code'):
                result_dict[token.name].append(lang.string)
        context['tokens'] = result_dict
        return context


@login_required
def translation_edit(request):
    context = {}
    token_name = request.GET.get('key', None)
    token = TokenLanguageString.objects.filter(name=token_name).first()
    if token:
        strings = LanguageString.objects.filter(token=token).select_related()
        codes = SystemLanguage.objects.all()
        context['codes'] = codes
        context['strings'] = strings
        context['translate_codes'] = [text.language_code.language_code for text in strings]
        context['key'] = token_name
    else:
        return HttpResponse('Key does not exist', status=404)
    if request.method == 'POST':
        codes = SystemLanguage.objects.all()
        html_parser = MyHTMLParser()
        for code in codes:
            value = request.POST.get(code.language_code, None)
            if value:
                value = value.strip()
                correct_html, error_msg = html_parser.feed(value)
                if not correct_html:
                    messages.error(request, 'Ошибка в html для языка {}, в тексте: "{}"\n. {}'.format(
                        code.language_code,
                        value,
                        error_msg, ))
                    return render(request, 'panel/translation_edit.html', context)
                try:
                    bot = SENDER_BOT.get_bot()
                    msg = bot.send_message(STORAGE_CHAT, value, parse_mode='html')
                    bot.delete_message(STORAGE_CHAT, msg.message_id)
                except:
                    messages.error(request,
                    'Ошибка в формате текста. Бот не сможет отправить сообщение с таким контекстом. Пожалуйста, проверьте отсутствие ошибок в служебных знаках и разметке.')
                    return render(request, 'panel/translation_edit.html', context)

                text = LanguageString.objects.filter(token=token, language_code=code).first()
                if text:
                    text.string = value
                    text.save()
                else:
                    new_language_string = LanguageString(language_code=code,
                                                         token=token,
                                                         string=value)
                    new_language_string.save()
        return redirect('panel:rows')

    return render(request, 'panel/translation_edit.html', context)


class AddLanguageView(FormView):
    template_name = 'panel/language_add.html'
    form_class = AddLanguageForm

    def form_valid(self, form):
        lang_code = form.cleaned_data.get('language_code', None)
        language = SystemLanguage.objects.filter(language_code=lang_code).first()
        if not language:
            new_language = SystemLanguage(language_code=lang_code)
            new_language.save()
            add_new_language_default(lang_code)
        self.success_url = reverse('panel:rows', None)
        return super().form_valid(form)


@login_required
def language_delete(request):
    if request.method == 'GET':
        if SystemLanguage.objects.all().count() > 1:
            language_code = request.GET.get('key', None)
            users = User.objects.filter(language=language_code).select_for_update()
            language_new = SystemLanguage.objects.first()
            users.update(
                language=language_new.language_code
            )
            language_del = SystemLanguage.objects.filter(language_code=language_code).first()
            if language_del:
                language_del.delete()
    return redirect('panel:rows')


def generate_graph_arriving_audience(langs, days, title):
    today = datetime.today() - timedelta(days=1)
    line_chart = pygal.Line(x_label_rotation=45)
    line_chart.title = title
    current_date = today - timedelta(days=days)
    all_users = []
    x_labels = []
    while current_date <= today:
        current_date += timedelta(days=1)
        all_users.append(User.objects.filter(date_joined__date=current_date, is_superuser=False).count())
        x_labels.append(current_date)
    if len(x_labels) > 200:
        x_labels_to_graph = x_labels[::10]
        for label in x_labels[::10]:
            for _ in range(9):
                x_labels_to_graph.insert(x_labels.index(label) + 1, None)
        x_labels = x_labels_to_graph
    elif len(x_labels) > 40:
        x_labels_to_graph = x_labels[::3]
        for label in x_labels[::3]:
            for _ in range(2):
                x_labels_to_graph.insert(x_labels.index(label) + 1, None)
        x_labels = x_labels_to_graph
    line_chart.x_labels = map(lambda d: d.strftime('%y.%m.%d') if d else None, x_labels)
    line_chart.add('Все', all_users)
    for lang in langs:
        current_date = today - timedelta(days=days)
        users_by_lang = []
        while current_date <= today:
            current_date += timedelta(days=1)
            users_by_lang.append(User.objects.filter(date_joined__date=current_date, is_superuser=False).count())
        line_chart.add(lang.language_code, users_by_lang)
    return line_chart.render_data_uri()


@login_required
def arriving_audience(request):
    langs = SystemLanguage.objects.all()
    context = {
        'graph_users_by_30_days': generate_graph_arriving_audience(langs, 30, 'Приплыв аудитории за последние 30 дней'),
    }
    return render(request, 'panel/arriving_audience.html', context)


@login_required
def arriving_audience_90_days(request):
    langs = SystemLanguage.objects.all()
    context = {
        'graph_users_by_90_days': generate_graph_arriving_audience(langs, 90, 'Приплыв аудитории за последние 90 дней'),
    }
    return render(request, 'panel/arriving_audience_90_days.html', context)


@login_required
def arriving_audience_365_days(request):
    langs = SystemLanguage.objects.all()
    context = {
        'graph_users_by_365_days': generate_graph_arriving_audience(langs, 365, 'Приплыв аудитории за последние 365 дней'),
    }
    return render(request, 'panel/arriving_audience_365_days.html', context)


def generate_graph_users(langs, days, title):
    today = datetime.today() - timedelta(days=1)
    line_chart = pygal.Line(x_label_rotation=45)
    line_chart.title = title
    current_date = today - timedelta(days=days)
    all_users = []
    x_labels = []
    while current_date <= today:
        current_date += timedelta(days=1)
        all_users.append(StatisticUser.objects.filter(
            date__day=current_date.day,
            date__month=current_date.month,
            date__year=current_date.year
        ).count())
        x_labels.append(current_date)
    if len(x_labels) > 200:
        x_labels_to_graph = x_labels[::10]
        for label in x_labels[::10]:
            for _ in range(9):
                x_labels_to_graph.insert(x_labels.index(label) + 1, None)
        x_labels = x_labels_to_graph
    elif len(x_labels) > 40:
        x_labels_to_graph = x_labels[::3]
        for label in x_labels[::3]:
            for _ in range(2):
                x_labels_to_graph.insert(x_labels.index(label) + 1, None)
        x_labels = x_labels_to_graph
    line_chart.x_labels = map(lambda d: d.strftime('%y.%m.%d') if d else None, x_labels)
    line_chart.add('Все', all_users)
    for lang in langs:
        current_date = today - timedelta(days=days)
        users_by_lang = []
        while current_date <= today:
            current_date += timedelta(days=1)
            users_by_lang.append(StatisticUser.objects.filter(
                date__day=current_date.day,
                date__month=current_date.month,
                date__year=current_date.year,
                user_language=lang.language_code
            ).count())
        line_chart.add(lang.language_code, users_by_lang)
    return line_chart.render_data_uri()


@login_required
def get_statistic(request):
    langs = SystemLanguage.objects.all()
    context = {
        'graph_users_by_30_days': generate_graph_users(langs, 30, 'Пользователи бота за последние 30 дней'),
    }
    return render(request, 'panel/statistics.html', context)


@login_required
def get_statistic_90_days(request):
    langs = SystemLanguage.objects.all()
    context = {
        'graph_users_by_90_days': generate_graph_users(langs, 90, 'Пользователи бота за последние 3 месяца'),
    }
    return render(request, 'panel/statistics_90_days.html', context)


@login_required
def get_statistic_365_days(request):
    langs = SystemLanguage.objects.all()
    context = {
        'graph_users_by_365_days': generate_graph_users(langs, 365, 'Пользователи бота за последний год'),
    }
    return render(request, 'panel/statistics_365_days.html', context)


@login_required
def current_statistic(request):
    users_count = User.objects.exclude(is_superuser=True).count()
    users_count_by_lang = []
    langs = SystemLanguage.objects.all()
    for lang in langs:
        count = User.objects.filter(language=lang.language_code).exclude(is_superuser=True).count()
        users_count_by_lang.append({
            'lang': lang.language_code,
            'count': count,
        })
    user_info = SENDER_BOT.get_blocked_stats()
    cur_blocked_count = user_info['blocked']
    deactivated_count = user_info['deleted']
    context = {
        'users_count': users_count,
        'users_count_by_lang': users_count_by_lang,
        'cur_blocked_count': cur_blocked_count,
        'cur_deactivated_count': deactivated_count,
    }
    return render(request, 'panel/current_statistic.html', context)


@login_required
def click_statistic(request):
    context = {
        'clicks': Click.objects.all(),
    }
    return render(request, 'panel/click_statistic.html', context)


@login_required
def artifact_statistic(request):
    context = {
        'artifacts': Artifact.objects.all(),
    }
    return render(request, 'panel/artifacts.html', context)

@login_required
def artifact_users(request):
    artifact_pk = request.GET.get('key', None)
    artifact = Artifact.objects.get(pk=artifact_pk)
    context = {
        'artifact': artifact,
        'users': artifact.users.all(),
    }
    return render(request, 'panel/artifact_users.html', context)

@login_required
def artifact_archive(request):
    artifact_name = request.GET.get('key', None)
    artifact = Artifact.objects.filter(name=artifact_name).first()
    if artifact:
        artifact.is_view = False
        artifact.save()
        context = {
            'artifacts': Artifact.objects.all(),
        }
        return render(request, 'panel/artifacts.html', context)


@login_required
def get_all_broadcast_messages(request):
    broadcasts = BroadCastSendTask.objects.all().order_by('-id')
    data = {'broadcasts': []}
    for broadcast in broadcasts:
        is_caption = False
        response = SENDER_BOT.get_task_response(broadcast.task_id)
        if response:
            if response.get('responses', False):
                photo_response = response['responses'][0][0].get('photo', None)
                document_response = response['responses'][0][0].get('document', None)
                video_response = response['responses'][0][0].get('video', None)
                audio_response = response['responses'][0][0].get('audio', None)
                if photo_response or document_response or video_response or audio_response:
                    is_caption = True
            data['broadcasts'].append(
                {
                    'broadcast': model_to_dict(broadcast),
                    'response': response,
                    'is_caption': is_caption,
                    'status': response['status'],
                }
            )
        else:
            data['broadcasts'].append(
                {
                    'broadcast': model_to_dict(broadcast),
                    'response': 'FAIL',
                    'is_caption': False,
                    'status': 'FAIL',
                }
            )
    return JsonResponse(data)


@login_required
@csrf_exempt
def edit_broadcast(request):
    task_id = request.POST.get('task_id')
    message = request.POST.get('message')
    is_caption = True if request.POST.get('is_caption') == 'true' else False
    SENDER_BOT.edit_broadcast(task_id, message, is_caption)
    broadcast = BroadCastSendTask.objects.filter(task_id=task_id).first()
    broadcast.message = message
    broadcast.save()
    return HttpResponse('ok', 200)


@login_required
@csrf_exempt
def remove_broadcast(request):
    task_id = request.POST.get('task_id')
    broadcast = BroadCastSendTask.objects.filter(task_id=task_id).first()
    broadcast.delete()
    SENDER_BOT.remove_broadcast(task_id)
    return HttpResponse('ok', 200)


class StateLinkCreateView(CreateView):
    template_name = 'panel/state_link.html'
    form_class = StateLinkForm
    model = StateLink

    def get_success_url(self):
        return reverse('panel:state_links')


class StateLinkListView(ListView):
    template_name = 'panel/state_links.html'
    model = StateLink
    paginate_by = 10
    context_object_name = 'state_links'


class StateLinkUpdateView(UpdateView):
    template_name = 'panel/state_link.html'
    form_class = StateLinkForm
    model = StateLink

    def get_success_url(self):
        return reverse('panel:state_links')


@login_required
def remove_state_link(request, pk):
    state = StateLink.objects.filter(pk=pk).first()
    state.delete()
    return redirect(reverse('panel:state_links'))


@csrf_exempt
@login_required
def user_history(request, pk):
    if request.method == 'GET':
        context = {}
        user = User.objects.get(pk=pk)
        user_id = user.user_id_int
        context['user'] = user
        messages = Message.objects.filter(user_id=user_id).order_by('-id')[:100]
        messages = reversed(messages)
        context['messages'] = messages
        return render(request, 'panel/user_history.html', context)
    if request.method == 'POST':
        message = request.POST.get('message', None)
        user_id = request.POST.get('user_id', None)
        user = User.objects.get(user_id_int=user_id)
        file_to_send = request.FILES.get('file_to_send', None)
        send_as_file = request.POST.get('send_as_file', False)
        if not message and not file_to_send:
            return HttpResponse('Невозможно отправить пустое сообщение без файла', status=501)
        if file_to_send is not None:
            bot = SENDER_BOT.get_bot()
            if file_to_send.content_type.find('audio/ogg') != -1:
                message_media = bot.send_audio(STORAGE_CHAT, file_to_send.file, title=str(file_to_send))
                response = bot.send_voice(
                    user_id,
                    message_media.voice.file_id,
                    message,
                )
            elif file_to_send.content_type.find('audio/') != -1:
                message_media = bot.send_audio(STORAGE_CHAT, file_to_send.file, title=str(file_to_send))
                response = bot.send_audio(
                    user_id,
                    message_media.audio.file_id,
                    message,
                )
            elif file_to_send.content_type.find('image/') != -1 and not send_as_file:
                message_media = bot.send_photo(STORAGE_CHAT, file_to_send.file)
                response = bot.send_photo(
                    user_id,
                    message_media.photo[-1].file_id,
                    message,
                )
            elif file_to_send.content_type.find('video/') != -1:
                message_media = bot.send_video(STORAGE_CHAT, file_to_send.file)
                response = bot.send_video(
                    user_id,
                    message_media.video.file_id,
                    message,
                )
            else:
                message_media = bot.send_document(STORAGE_CHAT, file_to_send.file)
                response = bot.send_document(
                    user_id,
                    message_media.document.file_id,
                    message,
                )
        else:
            SENDER_BOT.send_message(user_id, message)
        return redirect('panel:user_history', user.pk)
    
    
class ArtifactDeleteView(DeleteView):
    model = Artifact
    success_url = reverse_lazy('panel:artifact_statistic')

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)
    
@login_required
def artifacts_delete(request):
    Artifact.objects.all().delete()
    return redirect('panel:artifact_statistic')
