import json
from django.http import HttpResponse
from django_scaffold.settings import BASE_DIR
from django.shortcuts import render, redirect


def site_strings(request):
    with open(BASE_DIR + '/panel/modules/site_translations/ENG.json') as json_file:
        data = json.load(json_file)
        return render(request, 'panel/modules/site_translations/site_localization.html', {'data': data})


def site_translation_edit(request):
    context = {}
    key = request.GET.get('key', None)
    if key:
        with open(BASE_DIR + '/panel/modules/site_translations/ENG.json') as json_file:
            data = json.load(json_file)
        context['key'] = key
        context['text'] = data.get(key)
    else:
        return HttpResponse('Key does not exist', status=404)
    if request.method == 'POST':
        new_text = request.POST.get('text', None)
        data[key] = new_text
        with open(BASE_DIR + '/panel/modules/site_translations/ENG.json', 'w') as json_file:
            json.dump(data, json_file)
        return redirect('panel:site_rows')

    return render(request, 'panel/modules/site_translations/site_translation_edit.html', context)
