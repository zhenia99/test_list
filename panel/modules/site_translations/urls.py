from django.urls import path

from panel.modules.site_translations.views import site_strings, site_translation_edit

site_translations_urlpatterns = [
    path('site_translations', site_strings, name='site_rows'),
    path('site_translation_edit', site_translation_edit, name='site_translation_edit'),
]
