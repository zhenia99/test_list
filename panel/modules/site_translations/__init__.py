from panel.modules.site_translations.urls import site_translations_urlpatterns


def load_site_translations_panel_url() -> list:
    return site_translations_urlpatterns
