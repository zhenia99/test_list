from datetime import datetime
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.forms import model_to_dict
from main.models import SystemLanguage, ScheduledBroadCastSendTask, STATES


@login_required
def scheduled_sender(request):
    user_id = request.GET.get('user_id', None)
    languages = SystemLanguage.objects.all()
    context = dict()
    if user_id:
        context['user_id'] = user_id
    context['date_now'] = datetime.now().strftime("%Y-%m-%dT%H:%M")
    context['languages'] = languages
    context['states'] = STATES
    return render(request, 'panel/modules/scheduled_sender/scheduled_sender.html', context)


@login_required
@csrf_exempt
def remove_scheduled_broadcast(request):
    task_id = request.POST.get('task_id')
    ScheduledBroadCastSendTask.objects.get(pk=task_id).delete()
    return HttpResponse('ok', 200)


@login_required
def get_all_scheduled_broadcast_messages(request):
    scheduled_broadcast_list = ScheduledBroadCastSendTask.objects.filter(sent=False).order_by('-send_date')
    data = {'broadcasts': []}
    for scheduled_broadcast in scheduled_broadcast_list:
        data['broadcasts'].append(
            {
                'broadcast': model_to_dict(scheduled_broadcast, exclude=['targets']),
                'date_formatted': scheduled_broadcast.date_formatted,
                'target_count': scheduled_broadcast.targets_count,
                'is_caption': not scheduled_broadcast.file_to_send_id,
            }
        )
    return JsonResponse(data)
