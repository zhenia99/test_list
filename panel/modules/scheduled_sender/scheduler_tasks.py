from django.utils import timezone
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from main.models import ScheduledBroadCastSendTask, ScheduledBroadCastInlineButton, BroadCastSendTask
from panel.views import SENDER_BOT


def scheduled_broadcast():
    now = timezone.now()
    for scheduled_task in ScheduledBroadCastSendTask.objects.filter(sent=False, send_date__lte=now):
        keyboard = None
        if scheduled_task.have_keyboard:
            keyboard = InlineKeyboardMarkup(row_width=scheduled_task.keyboard_width)
            for button in ScheduledBroadCastInlineButton.objects.filter(scheduled_task=scheduled_task):
                if button.to_state_enabled:
                    keyboard.add(*[InlineKeyboardButton(text=button.text, callback_data=f'to_state;{button.state}')])
                else:
                    keyboard.add(*[InlineKeyboardButton(text=button.text, url=button.url)])
        targets = [u.user_id_int for u in scheduled_task.targets.all()]
        if scheduled_task.file_to_send_id is not None:
            if scheduled_task.file_type == 'audio':
                response = SENDER_BOT.send_audio(
                    targets,
                    scheduled_task.file_to_send_id,
                    scheduled_task.message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
            elif scheduled_task.file_type == 'image':
                response = SENDER_BOT.send_photo(
                    targets,
                    scheduled_task.file_to_send_id,
                    scheduled_task.message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
            elif scheduled_task.file_type == 'video':
                response = SENDER_BOT.send_video(
                    targets,
                    scheduled_task.file_to_send_id,
                    scheduled_task.message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
            else:
                response = SENDER_BOT.send_document(
                    targets,
                    scheduled_task.file_to_send_id,
                    scheduled_task.message,
                    priority=0,
                    broadcast=True,
                    reply_markup=keyboard
                )
        else:
            response = SENDER_BOT.send_message(
                targets,
                scheduled_task.message,
                priority=0,
                broadcast=True,
                reply_markup=keyboard
            )
        BroadCastSendTask.objects.create(task_id=response.text, message=scheduled_task.message)
        scheduled_task.sent = True
        scheduled_task.save(update_fields=['sent'])
