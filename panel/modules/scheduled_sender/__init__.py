from panel.modules.scheduled_sender.urls import scheduled_sender_urlpatterns


def load_scheduled_sender_panel_url() -> list:
    return scheduled_sender_urlpatterns
