from django.urls import path
from django.contrib.auth.decorators import login_required
from panel.modules.scheduled_sender.views import scheduled_sender, remove_scheduled_broadcast, get_all_scheduled_broadcast_messages

scheduled_sender_urlpatterns = [
    path('scheduled_sender', scheduled_sender, name='scheduled_sender'),
    path('remove_scheduled_broadcast', remove_scheduled_broadcast, name='remove_scheduled_broadcast'),
    path('get_all_scheduled_broadcast_messages', get_all_scheduled_broadcast_messages, name='get_all_scheduled_broadcast_messages'),
]
