from datetime import datetime
from django.contrib.auth.decorators import login_required
import pytz
from django.http import HttpResponse
from main.models import User
from panel.modules.user_to_excel.excel_utils import ExcelUserStats


@login_required()
def download_poll_pack_stats(request):
    user_list = User.objects.exclude(is_superuser=True)
    excel_poll = ExcelUserStats(user_list)
    excel_poll.user_list_sheet("Список пользователей")
    tz = pytz.timezone("Europe/Kiev")
    now = datetime.now(tz).strftime("%d_%m_%y")
    file_name = f"users_{now}.xlsx"
    stream = excel_poll.save_stream()
    response = HttpResponse(content=stream,
                            content_type='application/ms-excel')
    response['Content-Disposition'] = f'attachment; filename={file_name}'
    return response
