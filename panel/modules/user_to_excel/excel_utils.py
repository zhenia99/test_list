from tempfile import NamedTemporaryFile
import openpyxl
from openpyxl.cell import Cell
from openpyxl.styles import Border, Side
from openpyxl.utils import get_column_letter
from openpyxl.workbook.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet


class ExcelUserStats:
    def __init__(self, user_list):
        self.workbook: Workbook = openpyxl.Workbook()
        self.user_list = user_list

    def user_list_sheet(self, title):
        w_user = self.workbook.create_sheet(title=title)
        self.workbook.remove(self.workbook.get_sheet_by_name(self.workbook.get_sheet_names()[0]))
        first_row = ["ID", "Юзернейм", "Имя", "Фамилия", "Язык", "Заблокирован", "Дата добавления"]
        w_user.append(first_row)
        for i, user in enumerate(self.user_list, start=2):
            row = list()
            row.append(user.user_id_int)
            row.append(user.username_tg if user.username_tg else '')
            row.append(user.first_name if user.first_name else '')
            row.append(user.last_name if user.last_name else '')
            row.append(user.language)
            row.append('Да' if user.is_blocked else 'Нет')
            row.append(user.date_joined.date())
            w_user.append(row)

        self.adjust_columns(w_user, w_user.columns)
        self.adjust_rows(w_user, w_user.rows)
        self.set_borders_to_first_row(w_user, len(first_row))

    def adjust_columns(self, worksheet: Worksheet, columns):
        for col in columns:
            max_length = 0
            column = col[0].column
            for cell in col:
                try:
                    if len(str(cell.value)) > max_length:
                        max_length = len(cell.value)
                except:
                    pass
            adjusted_width = (max_length + 2) * 1.2
            if adjusted_width > 50:
                adjusted_width = 50
            if adjusted_width < 10:
                adjusted_width = 10
            worksheet.column_dimensions[get_column_letter(column)].width = adjusted_width

    def adjust_rows(self, worksheet: Worksheet, rows):
        for row in rows:
            max_height = 0
            for cell in row:
                try:
                    current_length = cell.value.count('\n')
                    if current_length > max_height:
                        max_height = current_length
                except:
                    pass
            adjusted_height = (max_height * 16)
            worksheet.row_dimensions[row[0].row].height = adjusted_height

    def set_border(self, cell: Cell):
        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))
        cell.border = thin_border

    def set_borders_to_first_row(self, worksheet: Worksheet, row_len: int):
        for column in range(1, row_len + 1):
            self.set_border(worksheet.cell(row=1, column=column))

    def save_excel(self, save_path):
        self.workbook.save(save_path)

    def save_stream(self):
        with NamedTemporaryFile() as tmp:
            self.workbook.save(tmp.name)
            tmp.seek(0)
            stream = tmp.read()
            return stream
