from panel.modules.user_to_excel.urls import user_to_excel_urlpatterns


def load_user_to_excel_panel_url() -> list:
    return user_to_excel_urlpatterns
