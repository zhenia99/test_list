from django.urls import path

from panel.modules.user_to_excel.views import download_poll_pack_stats

user_to_excel_urlpatterns = [
    path('poll_packs_stats/', download_poll_pack_stats, name='stats_poll_packs'),
]
