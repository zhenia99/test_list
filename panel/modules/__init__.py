from django_scaffold.settings import INSTALLED_MODULES
from panel.modules.scheduled_sender import load_scheduled_sender_panel_url
from panel.modules.site_translations import load_site_translations_panel_url
from panel.modules.user_to_excel import load_user_to_excel_panel_url


def load_modules_panel_url() -> list:
    urls = []
    if INSTALLED_MODULES.get('site_translations'):
        urls += load_site_translations_panel_url()
    if INSTALLED_MODULES.get('scheduled_sender'):
        urls += load_scheduled_sender_panel_url()
    if INSTALLED_MODULES.get('user_to_excel'):
        urls += load_user_to_excel_panel_url()
    return urls

