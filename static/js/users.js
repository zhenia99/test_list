$(document).ready(function () {
    var table = $('#table_users').DataTable({
        dom: "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>" +
            "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'r><'col-sm-12 col-md-4'f>>" +
            "<'row'<'col-sm-12't>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        columnDefs: [ {
                "targets": -1,
                "data": null,
                "render": function (data, type, row, meta) {
                    return '<a href=/admin?user_id='+data[0]+'>Send to</a>'
                }
            },
            {
                "targets": -2,
                "data": null,
                "render": function (data, type, row, meta) {
                    return '<a href=/users/edit?user_id='+data[0]+'>Edit</a>'
                }
            },
            {
                "targets": -3,
                "data": null,
                "render": function (data, type, row, meta) {
                    return '<input type="checkbox" '+(data[data.length-2] === 'True' ? 'checked' : '')+' disabled>'
                }
            },
            {
                "targets": [-1, -2, -3, -4],
                "orderable": false
            }],
        processing: true,
        serverSide: true,
        sPaginationType: "full_numbers",
        lengthMenu: [[50, 100, 150], [50, 100, 150]],
        scrollX: true,
        scrollY: "85%",
        ajax: '/users/serverside_table',
    });

    $("div.dataTables_filter input").unbind().keyup( function (e) {
        if (e.keyCode === 13) {
            table.search( this.value ).draw();
        }
    });
});
