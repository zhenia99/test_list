$("#msg_send_button").on("click", function(e) {
    e.preventDefault();
    if (!confirm('Вы действительно хотите выполнить рассылку сообщения?')){
        return;
    }

    const parseLocation = url => {
        let url_array = url.split('/');
        if (url_array[4] === 'panel'){
            return `/${url_array[3]}/panel/broadcast`;
        }
        else {
            return `/panel/broadcast`;
        }
    };

    $.ajax({
        url: parseLocation(location.href),
        type: 'post',
        data: new FormData($('#sendForm')[0]),
        contentType: false,
        processData: false,
        error: function(xhr, ajaxOptions, thrownError)
        {
            alert(xhr.responseText);
        },
        success: function () {
            $("#messageTextArea").val('');
            $("#file_to_send").val('');
            $("#users_list").val();
        }
    });
});

