'use strict';

const url = location.href;
const parseLocation = url => {
    let href = url.split('/').pop();
    // console.log(url);
    // console.log(url.split('/'));
    let url_array = url.split('/');
    if (href.includes('language_add') || href.includes('translation_edit')) href = 'translations';
    if (href.includes('users') || url.includes('user_edit')) href = 'users';
    if (url.includes('user_id')) href = '';
    if (url.includes('statistic')) href = 'statistic';
    if (url.includes('arriving_audience')) href = 'statistic';
    if (url.includes('arriving_audience_90_days')) href = 'statistic';
    if (url.includes('get_statistic_90_days')) href = 'statistic';
    if (url.includes('arriving_audience_365_days')) href = 'statistic';
    if (url.includes('get_statistic_365_days')) href = 'statistic';
    if (href.includes('create_state_link')) href = 'state_links';
    if (url.includes('update_state_link')) href = 'state_links';
    if (url_array[4] === 'panel'){
        return `/${url_array[3]}/panel/${href}`;
    }
    else {
        return `/panel/${href}`;
    }
};

const navLinks = Array.from(document.querySelectorAll('.navbar-nav .nav-link'));
const addActive = item => item.classList.add('active');
const removeActive = item => item.classList.remove('active');
const setActive = (url, arr) => {
    arr.forEach(item => {
        const itemHref = item.getAttribute('href');
        if (itemHref === url) {
            addActive(item);
        }else {
            removeActive(item);
        }
    });
};
setActive(parseLocation(url), navLinks);

