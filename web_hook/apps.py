from django.apps import AppConfig


class WebHookConfig(AppConfig):
    name = 'web_hook'
