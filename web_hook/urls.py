from django.urls import path

# from bot_scaffold_bot_django.settings import BOT_TOKEN
from django_scaffold.settings import BOT_TOKEN
from web_hook.views import get_message, remove

app_name = 'web_hook'

urlpatterns = [
    path(BOT_TOKEN, get_message, name='process_update'),
    # path('set', set_web_hook, name='set'),
    path('remove', remove, name='remove')
]
