import telebot
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
# from silk.profiling.profiler import silk_profile

from django_scaffold.settings import BOT_TOKEN
from main.bot.handler import bot_handler, BotHandlers
import json

from main.logger_settings import logger

BOT = BotHandlers(BOT_TOKEN)
# @silk_profile(name='webhook view')


@csrf_exempt
def get_message(request):
    if request.method == 'POST':
        try:
            json_list = json.loads(request.body.decode('utf-8'))
            updates = [telebot.types.Update.de_json(json_list)]
            BOT.bot.process_new_updates(updates)
        except:
            logger.exception("Error!")
        return HttpResponse("!", status=200)
    return HttpResponse("Are you kidding me?", status=404)


@staff_member_required
def remove(request):
    bot_handler.bot.remove_webhook()
    return redirect('/')

