#Sender API

| API Method      | Send messages
|-----------------| ---
| URL             | `/api/send_to_user`
| Method          | `POST`
| Params          | `targets=list[str or int], message=str, parse_mode=str, token=str, keyboard_str=pickled_keyboard_object, [priority=int], [document_type=str], [document=file], [document_id=str], [send_to_all=bool],`
| Success Response| `Task_id`
| Error Response  | `'Missing parameters' or 'Wrong parse mode'`

| API Method      | Get sender status
|-----------------| ---
| URL             | `/api/get_status`
| Method          | `GET`
| Params          | `bot_id=str (bot token)`
| Success Response| `[percentage_of_sent_messages=float, sent/total_messages=str]`
| Error Response  | `'Missing parameters' or [0, '-1/0'] if bot info is missing`

| API Method      | Get blocked/deactivated users statistic
|-----------------| ---
| URL             | `/api/get_statistic`
| Method          | `GET`
| Params          | `bot_id=str (bot token)`
| Success Response| `{blocked_count: int, deactivated_count: int}`
| Error Response  | `'Missing parameters' or {error: 1}`
