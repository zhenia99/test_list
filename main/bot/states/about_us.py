from main.bot.keyboards import get_about_us_keyboard
from main.bot.states import base_state
from main.translations import get_translation_for

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

class AboutUs(base_state.BaseState):
    def __init__(self):
        super().__init__()
        self._buttons = {
            'back_btn': self.cancel,
        }
        self._base_keyboard = get_about_us_keyboard

    def entry(self, message, user, sender: SenderClient):
        sender.send_message(message.chat.id,
                            get_translation_for(user.language, 'about_msg'),
                            reply_markup=self._base_keyboard(user.language))
        return base_state.RET_OK, None, None, None

    def cancel(self, message, user, sender: SenderClient):
        return base_state.RET_GO_TO_STATE, 'MenuState', message, user
