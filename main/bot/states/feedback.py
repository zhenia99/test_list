from telebot import types
from telebot.types import Message

from django_scaffold.settings import ADMIN_CHAT
from main.bot.states import base_state
from main.models import FeedbackMsg, User
from main.translations import get_translation_for

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

class FeedbackState(base_state.BaseState):
    def __init__(self):
        super().__init__()
        self._buttons = {
            'back_btn': self.cancel,
        }
        self._base_keyboard = self.get_keyboard

    def get_keyboard(self, language='ru'):
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        keyboard.add(get_translation_for(language, 'ready_btn'), get_translation_for(language, 'back_btn'))
        return keyboard

    def entry(self, message, user, sender: SenderClient):
        sender.send_message(message.chat.id,
                            get_translation_for(user.language, 'feedback_msg'),
                            reply_markup=self._base_keyboard(user.language))
        return base_state.RET_OK, None, None, None

    def process_message(self, message: Message, user: User, sender: SenderClient):
        template_answer = get_translation_for('ru', 'template_for_admin_chat_from_user').format(
            user.user_id_int,
            user.username,
            message.text if message.text else message.caption
        )
        msg_to_admin = None
        if message.text == "Готово":
            return super(FeedbackState, self).back_button(message, user, sender)
        if message.text:
            msg_to_admin = sender.get_bot().send_message(ADMIN_CHAT,
                                                         template_answer, parse_mode='html')
        elif message.photo:
            msg_to_admin = sender.get_bot().send_photo(ADMIN_CHAT, message.photo[-1].file_id,
                                                       caption=template_answer, parse_mode='HTML')
        elif message.video:
            msg_to_admin = sender.get_bot().send_video(ADMIN_CHAT, message.video.file_id,
                                                       caption=template_answer, parse_mode='HTML')
        elif message.audio:
            msg_to_admin = sender.get_bot().send_audio(ADMIN_CHAT, message.audio.file_id,
                                                       caption=template_answer, parse_mode='HTML')
        elif message.voice:
            msg_to_admin = sender.get_bot().send_voice(ADMIN_CHAT, message.voice.file_id,
                                                       caption=template_answer, parse_mode='HTML')
        elif message.document:
            msg_to_admin = sender.get_bot().send_document(ADMIN_CHAT, message.document.file_id,
                                                          caption=template_answer, parse_mode='HTML')
        if msg_to_admin:
            FeedbackMsg.objects.create(user=user, msg_id=message.message_id, msg_id_admin=msg_to_admin.message_id)
        sender.send_message(message.chat.id,
                            get_translation_for(user.language, 'your_message_to_us_send_done'),
                            reply_markup=self._base_keyboard(user.language))

    def cancel(self, message, user, sender: SenderClient):
        return base_state.RET_GO_TO_STATE, 'MyProject', message, user

