from django_scaffold.settings import ADMIN_CHAT
from main.bot.keyboards import get_main_menu_keyboard
from main.bot.states import base_state
from main.translations import get_translation_for

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

class MenuState(base_state.BaseState):

    def __init__(self):
        super().__init__()
        self._buttons = {
            'rename_user': self.rename_user_btn,
            'about_btn': self.about_us__btn,
            'project_btn': self.my_project_btn,
            'feedback_btn': self.feedback_btn,
        }
        self._base_keyboard = get_main_menu_keyboard

    def entry(self, message, user, sender: SenderClient):
        sender.send_message(message.chat.id,
                            get_translation_for(user.language, 'menu_msg'),
                            reply_markup=self._base_keyboard(user.language)
                            )
        return base_state.RET_OK, None, None, None

    def about_us__btn(self, message, user, sender: SenderClient):
        return base_state.RET_GO_TO_STATE, 'AboutUs', message, user

    def my_project_btn(self, message, user, sender: SenderClient):
        return base_state.RET_GO_TO_STATE, 'MyProject', message, user

    def rename_user_btn(self, message, user, sender: SenderClient):
        return base_state.RET_GO_TO_STATE, 'ChangeNickState', message, user

    def feedback_btn(self, message, user, sender: SenderClient):
        if ADMIN_CHAT:
            return base_state.RET_GO_TO_STATE, 'FeedbackState', message, user
        else:
            sender.send_message(message.chat.id,
                                get_translation_for(user.language, 'use_keyboard_msg'),
                                reply_markup=self._base_keyboard(user.language)
                                )
            return base_state.RET_OK, None, None, None
