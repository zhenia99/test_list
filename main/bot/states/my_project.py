from main.bot.keyboards import get_my_project_keyboard
from main.bot.states import base_state
from main.translations import get_translation_for
from django_scaffold.settings import ADMIN_CHAT

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

class MyProject(base_state.BaseState):
    def __init__(self):
        super().__init__()
        self._base_keyboard = get_my_project_keyboard

    def entry(self, message, user, sender: SenderClient):
        sender.send_message(message.chat.id,
                            get_translation_for(user.language, 'my_project_msg'),
                            reply_markup=self._base_keyboard(user.language))
        return base_state.RET_OK, None, None, None

    def process_callback(self, callback, user, sender: SenderClient):
        if callback.data=='feedback':
            if ADMIN_CHAT:
                return base_state.RET_GO_TO_STATE, 'FeedbackState', callback.message, user
            else:
                sender.send_message(callback.message.chat.id,
                                    get_translation_for(user.language, 'use_keyboard_msg'),
                                    reply_markup=self._base_keyboard(user.language)
                                    )
                return base_state.RET_OK, None, None, None
        elif callback.data == 'cancel':
            return base_state.RET_GO_TO_STATE, 'MenuState', callback.message, user
        elif callback.data == 'add_admin':
            return base_state.RET_GO_TO_STATE, 'MyProject', callback.message, user