from telebot.types import Message

from main.bot.statistic import register_click, register_message
from main.translations import get_translation_for
from main.models import State, User

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient
    
RET_OK = 1
RET_GO_TO_STATE = 2
RET_ANSWER_CALLBACK = 3
RET_ANSWER_AND_GO_TO_STATE = 4


def user_last_state_model(user):
    state = State.objects.filter(user=user).last()
    return state


class BaseState(object):
    has_entry = True
    accepts_everything = False

    def __init__(self):
        self._buttons = {
            'ready_btn': self.back_button,
        }
        self._base_keyboard = None

    def entry(self, message: Message, user: User, sender: SenderClient):
        return RET_OK, 0, None, None

    def process_message_with_buttons(self, message: Message, user: User, sender: SenderClient):
        message_text = message.text
        if message_text:
            if message_text == 'Info about Developers':
                return self.developers_msg(message, user, sender)
            for k, v in self._buttons.items():
                assert user.language is not None
                # bot_from_db = Bot.objects.filter(token=sender.get_bot().token)
                if message_text == get_translation_for(user.language, k):
                    ret = v(message, user, sender)
                    if isinstance(ret, tuple):
                        register_click(k)
                        return ret
                    break
        return self.process_message(message, user, sender)

    def process_message(self, message: Message, user: User, sender: SenderClient):
        text = get_translation_for(user.language, 'use_keyboard_msg')
        register_message(message.text, user)
        sender.send_message(message.chat.id,
                            text,
                            reply_markup=self._base_keyboard(user.language)
                            if self._base_keyboard else None
                            )
        return RET_OK, None, None, None

    def process_callback(self, callback, user, sender: SenderClient):
        sender.answer_callback_query(callback.id, get_translation_for(user.language,
                                                                      'not_correct_state_for_inline_msg'),
                                     show_alert=True)
        return RET_OK, None, None, None

    # Buttons

    def back_button(self, message: Message, user: User, sender: SenderClient):
        last_state = user_last_state_model(user)
        if last_state:
            last_state.delete()
            move_to = user_last_state_model(user)
            move_to.delete()
            if move_to:
                return RET_GO_TO_STATE, move_to.name, message, user
        return RET_GO_TO_STATE, 'MenuState', message, user

    def developers_msg(self, message: Message, user: User, sender: SenderClient):
        sender.send_message(
            message.chat.id,
            'Developed by Mometum bots.\nmomentum-bots.top'
        )
        return RET_OK, None, None, None
