from main.bot.keyboards import get_change_nick_keyboard
from main.bot.states import base_state
from main.bot.utils import is_valid_name
from main.translations import get_translation_for

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

class ChangeNickState(base_state.BaseState):
    def __init__(self):
        super().__init__()
        self._buttons = {
            'cancel': self.cancel,
        }
        self._base_keyboard = get_change_nick_keyboard

    def entry(self, message, user, sender: SenderClient):
        sender.send_message(message.chat.id,
                            get_translation_for(user.language, 'new_name_request_msg'),
                            reply_markup=self._base_keyboard(user.language))
        return base_state.RET_OK, None, None, None

    def process_message(self, message, user, sender: SenderClient):
        if is_valid_name(message.text):
            user.name = message.text
            user.save()
            sender.send_message(message.chat.id,
                                'Имя было сменено на: {}'.format(user.name),
                                reply_markup=self._base_keyboard(user.language))
            return base_state.RET_GO_TO_STATE, 'MenuState', message, user
        else:
            sender.send_message(message.chat.id,
                                get_translation_for(user.language, 'new_name_error_msg'),
                                reply_markup=self._base_keyboard(user.language))
            return base_state.RET_OK, None, None, None

    def cancel(self, message, user, sender: SenderClient):
        return base_state.RET_GO_TO_STATE, 'MenuState', message, user
