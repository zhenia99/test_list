from main.bot.keyboards import get_choose_language_keyboard
from main.bot.states import base_state
from main.bot.statistic import register_click, register_message
from main.translations import get_translation_for
from main.models import LanguageString, TokenLanguageString

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

class ChooseLanguageState(base_state.BaseState):
    def __init__(self):
        super().__init__()
        self._base_keyboard = get_choose_language_keyboard

    def entry(self, message, user, sender: SenderClient):
        message_answer = ''
        token = TokenLanguageString.objects.filter(name='choose_language_msg').first()
        languages = LanguageString.objects.filter(token=token)
        if languages:
            for language in languages:
                message_answer += language.string + '\n'
            sender.send_message(message.chat.id,
                                message_answer,
                                reply_markup=self._base_keyboard(user.language))
            return base_state.RET_OK, None, None, None

    def process_message(self, message, user, sender: SenderClient):
        lang_string = LanguageString.objects.filter(string=message.text).select_related().first()
        if lang_string:
            register_click(lang_string.token.name)
            user.language = lang_string.language_code.language_code
            user.save()
            return base_state.RET_GO_TO_STATE, 'MenuState', message, user

        else:
            register_message(message.text, user)
            sender.send_message(message.chat.id, get_translation_for(user.language, 'use_keyboard_msg'),
                                reply_markup=self._base_keyboard(user.language))
            return base_state.RET_OK, None, None, None

