from main.bot.states import base_state
from main.models import SystemLanguage, StateLink, UsersStateLinks
from main.translations import get_translation_for

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

class StartState(base_state.BaseState):
    has_entry = False

    def process_message(self, message, user, sender: SenderClient):
        ref_id = message.text.replace('/start', '').split()
        if ref_id:
            state_link = ref_id[0]
            state_link = StateLink.objects.filter(state_link_id=state_link).first()
            if INSTALLED_MODULES.get('statelinks'):
                if state_link:
                    state_name = state_link.state
                    user.go_from_link_to_poll = True
                    user.save()
                    user.selected_state_link = state_link
                    users_statelink = UsersStateLinks.objects.filter(user=user, selected_state_link=state_link).first()
                    if not users_statelink:
                        users_statelink = UsersStateLinks(user=user, selected_state_link=state_link)
                        users_statelink.save()
                        state_link.statistic_count += 1
                        state_link.save()
                    return base_state.RET_GO_TO_STATE, state_name, message, user

        sender.send_message(message.chat.id,
                            get_translation_for(user.language, 'welcome_msg')
                            .format(user.first_name)
                            )

        count_languages = SystemLanguage.objects.count()
        if count_languages > 1:
            return base_state.RET_GO_TO_STATE, 'ChooseLanguageState', message, user
        return base_state.RET_GO_TO_STATE, 'MenuState', message, user
