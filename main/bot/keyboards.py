from telebot import types

from django_scaffold.settings import ADMIN_CHAT
from main.translations import get_translation_for
from main.models import LanguageString, TokenLanguageString


def get_change_nick_keyboard(language='ru'):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(get_translation_for(language, 'cancel'))
    return keyboard


def get_about_us_keyboard(language='ru'):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(get_translation_for(language, 'back_btn'))
    return keyboard


def get_my_project_keyboard(language='ru'):
    keyboard = types.InlineKeyboardMarkup()
    feedback = types.InlineKeyboardButton(get_translation_for(language, 'feedback_inline'), callback_data='feedback')
    add_admin = types.InlineKeyboardButton(get_translation_for(language, 'add_admin_inline'), callback_data='add_admin')
    back = types.InlineKeyboardButton(get_translation_for(language, 'cancel'), callback_data='cancel')
    keyboard.add(feedback, add_admin)
    keyboard.row(back)
    return keyboard


def get_main_menu_keyboard(language='ru'):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    keyboard.add(get_translation_for(language, 'about_btn'), get_translation_for(language, 'project_btn'))
    return keyboard


def get_choose_language_keyboard(language='ru'):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    token = TokenLanguageString.objects.filter(name='choose_language_btn').first()
    languages = LanguageString.objects.filter(token=token)
    for language in languages:
        keyboard.add(language.string)
    return keyboard


def remove_keyboard(language='ru'):
    keyboard = types.ReplyKeyboardRemove()
    return keyboard
