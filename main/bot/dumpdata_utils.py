import os
from datetime import datetime
from main.logger_settings import logger
from django_scaffold.settings import BOT_TOKEN
from main.bot.sender_client import SenderClient
from django.core.management import call_command

bot_name = SenderClient(BOT_TOKEN).get_bot().get_me().username
dir_name = f'dumps/{bot_name}_dumps'


def make_dump_data(name_day=''):
    try:
        # not rename file!!!
        with open('{}/dump_{}_{}_{}.json'.format(dir_name, bot_name, name_day,
                                                 datetime.now().date()), 'w') as f:
            call_command('dumpdata', indent=2, stdout=f)
        clean_old_dump_data(name_day.split('_')[-1], str(datetime.now().date()))
    except:
        logger.exception('Error!')


def clean_old_dump_data(name_day, date):
    try:
        files = os.listdir(f'{dir_name}')
        for file in files:
            date_file_split = file.split('_')
            if len(date_file_split) > 3:
                date_file = date_file_split[-1].split('.')[0]
                name_file_day = date_file_split[-2]
                if str(date_file) != date and str(name_file_day) == name_day:
                    os.remove(f'{dir_name}/{file}')
    except:
        logger.exception('Error!')


def check_actual_dump():
    create_target_directory_if_not_exist()
    files = os.listdir(f'{dir_name}')
    files_date = []
    for file in files:
        date_file_split = file.split('_')
        if len(date_file_split) > 3:
            date_file = date_file_split[-1].split('.')[0]
            name_file_day = date_file_split[-2]
            files_date.append({name_file_day: date_file})

    dumps_days = {'day': 1, 'twoDay': 2, 'threeDay': 3, 'week': 7, 'month': 30}
    all_file_days = set().union(*(d.keys() for d in files_date))
    for dump_day in dumps_days:
        if dump_day not in all_file_days:
            make_dump_data(f'last_{dump_day}')
        else:
            result = list((obj.get(dump_day) for obj in files_date))
            result = [x for x in result if x]
            if (datetime.now() - datetime.strptime(result[0], '%Y-%m-%d')).days >= dumps_days.get(dump_day):
                make_dump_data(f'last_{dump_day}')


def create_target_directory_if_not_exist():
    if not os.path.exists(f'{dir_name}'):
        os.makedirs(f'{dir_name}')
