import pickle
import re
from datetime import datetime, timedelta
from threading import Thread

from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from django.db.models.fields.files import FieldFile

from django_scaffold.settings import CONNECTION_REDIS_DDOS, COUNT_OF_CLICKS
from main.logger_settings import logger
from main.models import StatisticUser, StatisticLanguageDay

DDOS_MESSAGE = 'Помічена підозріла активність, не натискайте нічого протягом 10 секунд і ви знову зможете ' \
               'користуватися ботом.'


def is_valid_name(name):
    pattern = r'[a-zA-Z]{2,20}\s[a-zA-Z]{2,20}|' \
              r'[а-яА-ЯіІїЇґҐєЄ]{3,20}\s[а-яА-ЯіІїЇґҐєЄ]{3,20}|' \
              r'[а-яА-ЯіІїЇґҐєЄ]{3,20}|[a-zA-Z]{2,20}'
    return True if re.fullmatch(pattern, name) else False


def is_valid_age(age):
    return True if age.isdigit() else False


def is_valid_phone_number(phone):
    pattern = r'[0-9\+]{5,15}|[0-9]{5,15}'
    return True if re.fullmatch(pattern, phone) else False


def is_email(string):
    from django.core.exceptions import ValidationError
    from django.core.validators import EmailValidator

    validator = EmailValidator()
    try:
        validator(string)
    except ValidationError:
        return False
    return True


def is_user_ddosing(chat_id, tg_user, bot):
    ddos_data_bytes = CONNECTION_REDIS_DDOS.get(tg_user.id)
    if ddos_data_bytes:
        now = datetime.now()
        data = pickle.loads(ddos_data_bytes)
        if (data['update_date'] < (now - timedelta(seconds=10))) and data['count_click'] >= COUNT_OF_CLICKS:
            data['update_date'] = now
            data['count_click'] = 1
            data['is_send_msg'] = False
            CONNECTION_REDIS_DDOS.set(tg_user.id, pickle.dumps(data))
        elif (data['update_date'] < (now - timedelta(seconds=1))) and data['count_click'] < COUNT_OF_CLICKS:
            data['update_date'] = now
            data['count_click'] = 1
            CONNECTION_REDIS_DDOS.set(tg_user.id, pickle.dumps(data))
        else:
            if data['count_click'] >= COUNT_OF_CLICKS:
                data['update_date'] = now
                if not data['is_send_msg']:
                    Thread(target=bot.send_message, args=(chat_id, DDOS_MESSAGE)).start()
                    data['is_send_msg'] = True
                CONNECTION_REDIS_DDOS.set(tg_user.id, pickle.dumps(data))
                return True
            elif data['update_date'] > (now - timedelta(seconds=1)):
                data['update_date'] = now
                data['count_click'] += 1
                CONNECTION_REDIS_DDOS.set(tg_user.id, pickle.dumps(data))
    else:
        data = {
            'update_date': datetime.now(),
            'count_click': 1,
            'is_send_msg': False
        }
        CONNECTION_REDIS_DDOS.set(tg_user.id, pickle.dumps(data), ex=60 * 5)
    return False


def validate_image(file_obj: FieldFile):
    try:
        a = get_image_dimensions(file_obj)
    except Exception:
        try:
            logger.exception(f'Error with validate image. {file_obj.name} ({file_obj.path})')
        except:
            logger.exception('error with logging')
        return
    h, w = a
    if not h or not w:
        return
    dimension = w / h if w > h else h / w
    if dimension >= 20:
        raise ValidationError(f"Соотношение сторон фото должно быть больше, чем 1 к 20. У вас: 1 к {round(dimension)}")


def cache_stats():
    uncached_stats = StatisticUser.objects.filter(cached = False)
    if uncached_stats:
        print("==Stated caching started==")
        for stat in uncached_stats:
            statscounter = StatisticLanguageDay.objects.filter(date=stat.date, language=user_language).first()
            if not statscounter:
                StatisticLanguageDay.objects.create(date=stat.date, language=user_language)
            else:
                statscounter.count += 1
                statscounter.save()
            stat.cached = True
            stat.save()
        print("==Caching ended==")

