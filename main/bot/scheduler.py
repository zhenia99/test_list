from time import sleep
import schedule

from main.bot.dumpdata_utils import check_actual_dump
from main.logger_settings import logger
from django_scaffold.settings import DEVELOP

from panel.modules.scheduled_sender.scheduler_tasks import scheduled_broadcast

def run_scheduler():
    if not DEVELOP:
        schedule.every(1).days.at('01:00').do(check_actual_dump)
    schedule.every(30).seconds.do(scheduled_broadcast)

    while True:
        try:
            schedule.run_pending()
        except:
            logger.exception('Error!')
        sleep(1)
