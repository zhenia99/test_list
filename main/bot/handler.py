from datetime import datetime

from telebot import types

from django_scaffold.settings import STATES_HISTORY_LEN, DEVELOP, ADMIN_CHAT, BOT_TOKEN
from main.bot.feedback_utils import answer_from_user_to_admin, answer_from_admin_chat
from main.bot.states.change_nick import ChangeNickState
from main.bot.states.about_us import AboutUs
from main.bot.states.my_project import MyProject
from main.bot.states.choose_language import ChooseLanguageState
from main.bot.states.feedback import FeedbackState
from main.bot.utils import is_user_ddosing
from main.models import User, State, StatisticUser, FeedbackFromAdminMsg
from main.logger_settings import logger
from main.bot.states import base_state
from main.bot.states.start import StartState
from main.bot.states.menu import MenuState
from main.translations import get_translation_for
from telebot.types import Message, CallbackQuery, PreCheckoutQuery
import telebot

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
    from main.models import Message as MessageModel, File
else:
    from main.bot.sender_client import SenderClient


def user_last_state(user, default):
    state = State.objects.filter(user=user).select_related().last()
    if state:
        return state.name
    return default


class BotHandlers(object):
    def __init__(self, token=BOT_TOKEN):
        self.bot = telebot.TeleBot(token, threaded=False)
        self.sender = SenderClient(token)
        self.__states = {}
        self.__start_state = 'StartState'
        self.__register_states(
            StartState,
            MenuState,
            ChangeNickState,
            AboutUs,
            MyProject,
            ChooseLanguageState,
            FeedbackState,
        )
        self.__start_handling()
        self._base_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)

    def __register_event(self, user):
        today = datetime.today()
        statistic = StatisticUser.objects.filter(
            user=user,
            date__day=today.day,
            date__month=today.month,
            date__year=today.year
        ).first()
        if not statistic:
            StatisticUser(date=today, user=user, user_language=user.language).save()
        elif statistic.user_language != user.language:
            statistic.user_language = user.language
            statistic.save()

    def __get_or_register_user(self, chat_id, tg_user):
        # DDOS protect
        if is_user_ddosing(chat_id, tg_user, self.bot):
            return None
        user = User.objects.filter(user_id_int=tg_user.id).select_related().first()
        if user is None:
            user = User(first_name=tg_user.first_name,
                        last_name=tg_user.last_name if tg_user.last_name else tg_user.first_name)
            user.username = tg_user.username if tg_user.username else tg_user.first_name + str(tg_user.id)
            user.user_id_int = tg_user.id
            user.user_id_s = str(tg_user.id)
            user.username_tg = tg_user.username
            user.save()
            state = State(
                name=self.__start_state,
                user=user
            )
            state.save()
        else:
            user.first_name = tg_user.first_name
            user.last_name = tg_user.last_name if tg_user.last_name else tg_user.first_name
            user.username = tg_user.username if tg_user.username else tg_user.first_name + str(tg_user.id)
            user.username_tg = tg_user.username
            user.save()
        self.__register_event(user)
        return user

    def __start_handling(self):

        def for_private_chats_only(message):
            if isinstance(message, Message):
                if message.chat.type != 'private':
                    return False
            elif isinstance(message, CallbackQuery):
                if message.message.chat.type != 'private':
                    return False
            return True

        def for_public_chats_only(message):
            return not for_private_chats_only(message)

        def blocked_check(chat, user):
            if user.is_blocked:
                self.sender.send_message(chat.id,
                                         get_translation_for(user.language, 'blocked_msg'),
                                         priority=0)
                return True

        def reply_from_admin_chat(message):
            return str(message.chat.id) == ADMIN_CHAT and message.reply_to_message

        def check_reply_to_admin_msg(message):
            if message.reply_to_message:
                msg_from_admin = FeedbackFromAdminMsg.objects.filter(msg_id=message.reply_to_message.message_id,
                                                                     user__user_id_int=int(message.chat.id)).first()
                if msg_from_admin:
                    return True
            return False

        @self.bot.pre_checkout_query_handler(func=lambda query: True)
        def checkout(pre_checkout_query: PreCheckoutQuery):
            self.bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True,
                                               error_message="Aliens tried to steal your card's CVV, but we "
                                                             "successfully "
                                                             "protected your credentials, "
                                                             " try to pay again in a few minutes, "
                                                             "we need a small rest.")
            self.__process_ret_code((base_state.RET_OK, None, None, None))

        @self.bot.message_handler(content_types=['successful_payment'])
        def got_payment(message_from_bot: Message):
            user = self.__get_or_register_user(message_from_bot.from_user.id, message_from_bot.from_user)
            self.__process_ret_code((base_state.RET_GO_TO_STATE, 'MenuState', message_from_bot, user))

        @self.bot.message_handler(commands=['start'], func=for_private_chats_only)
        def send_welcome(message):
            try:
                user = self.__get_or_register_user(message.chat.id, message.from_user)

                if user is None or blocked_check(message.chat, user):
                    return
                State(name=self.__start_state, user=user).save()

                self.__process_message(message, user)
            except:
                logger.exception("Error!")

        @self.bot.message_handler(content_types=['text'], func=for_private_chats_only)
        def text_handler(message):
            try:
                user = self.__get_or_register_user(message.chat.id, message.from_user)
                if user is None or blocked_check(message.chat, user):
                    return
                if INSTALLED_MODULES.get('user_history'):
                    MessageModel.objects.create(
                        user_id = user.user_id_int,
                        message = message.text,
                        from_user = True
                    )
                if not check_reply_to_admin_msg(message):
                    self.__process_message(message, user)
                else:
                    answer_from_user_to_admin(user, message, self.bot)
            except:
                logger.exception("Error!")

        @self.bot.message_handler(content_types=['text', 'photo', 'audio', 'document', 'video', 'video_note',
                                                 'voice', 'contact', 'location', 'venue'],
                                  func=reply_from_admin_chat)
        def reply_admin_handler(message):
            try:
                user = self.__get_or_register_user(message.from_user.id, message.from_user)
                if user is None or blocked_check(message.chat, user):
                    return
                answer_from_admin_chat(message, self.bot)
            except:
                logger.exception("Error!")

        @self.bot.message_handler(content_types=['audio', 'document', 'photo', 'sticker', 'video', 'video_note',
                                                 'voice'], func=for_private_chats_only)
        def everything_handler(message):
            try:
                user = self.__get_or_register_user(message.chat.id, message.from_user)
                if user is None or blocked_check(message.chat, user):
                    return
                if INSTALLED_MODULES.get('user_history'):
                    if message.content_type == 'photo':
                        file_id = message.photo[-1].file_id
                        if not File.objects.filter(file_id=file_id).exists():
                            tg_file = self.bot.get_file(file_id)
                            ext = tg_file.file_path.split('.')[-1]
                            downloaded_file = self.bot.download_file(tg_file.file_path)
                            
                            with open(f'{file_id}.{ext}','wb') as new_file:
                                new_file.write(downloaded_file)
                                
                            local_file = open(f'{file_id}.{ext}', 'rb')
                            djangofile = DjangoFile(local_file)
                            
                            savedfile = File()
                            savedfile.file_id = file_id
                            savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                            
                            os.remove(f'{file_id}.{ext}')
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            file_id = file_id,
                            filetype = 'photo'
                        )
                    if message.content_type == 'video':
                        file_id = message.video.file_id
                        if not File.objects.filter(file_id=file_id).exists():
                            tg_file = self.bot.get_file(file_id)
                            ext = tg_file.file_path.split('.')[-1]
                            downloaded_file = self.bot.download_file(tg_file.file_path)
                            
                            with open(f'{file_id}.{ext}','wb') as new_file:
                                new_file.write(downloaded_file)
                                
                            local_file = open(f'{file_id}.{ext}', 'rb')
                            djangofile = DjangoFile(local_file)
                            
                            savedfile = File()
                            savedfile.file_id = file_id
                            savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                            
                            os.remove(f'{file_id}.{ext}')
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            filetype = 'video',
                            file_id= file_id
                        )
                    if message.content_type == 'audio':
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            filetype = 'audio'
                        )
                    if message.content_type == 'document':
                        file_id = message.document.file_id
                        if not File.objects.filter(file_id=file_id).exists():
                            tg_file = self.bot.get_file(file_id)
                            ext = tg_file.file_path.split('.')[-1]
                            downloaded_file = self.bot.download_file(tg_file.file_path)
                            
                            with open(f'{file_id}.{ext}','wb') as new_file:
                                new_file.write(downloaded_file)
                                
                            local_file = open(f'{file_id}.{ext}', 'rb')
                            djangofile = DjangoFile(local_file)
                            
                            savedfile = File()
                            savedfile.file_id = file_id
                            savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                            
                            os.remove(f'{file_id}.{ext}')
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            file_id = file_id,
                            filetype = 'document'
                        )
                    if message.content_type == 'sticker':
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            filetype = 'sticker'
                        )
                    if message.content_type == 'video_note':
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            filetype = 'video_note'
                        )
                    if message.content_type == 'contact':
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            filetype = 'contact'
                        )
                    if message.content_type == 'location':
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            filetype = 'location'
                        )
                    if message.content_type == 'voice':
                        file_id = message.voice.file_id
                        if not File.objects.filter(file_id=file_id).exists():
                            tg_file = self.bot.get_file(file_id)
                            ext = tg_file.file_path.split('.')[-1]
                            downloaded_file = self.bot.download_file(tg_file.file_path)
                            
                            with open(f'{file_id}.{ext}','wb') as new_file:
                                new_file.write(downloaded_file)
                                
                            local_file = open(f'{file_id}.{ext}', 'rb')
                            djangofile = DjangoFile(local_file)
                            
                            savedfile = File()
                            savedfile.file_id = file_id
                            savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                            
                            os.remove(f'{file_id}.{ext}')
                        MessageModel.objects.create(
                            user_id = user.user_id_int,
                            message = message.caption,
                            from_user = True, 
                            file_id = file_id,
                            filetype = 'voice'
                        )
                state_correct, current_state, c_name = self.__get_state(user_last_state(user, self.__start_state))
                if not state_correct:
                    State(name=c_name, user=user).save()
                    self.__process_message(message, user)
                else:
                    if current_state.accepts_everything:
                        self.__process_message(message, user)
                    else:
                        self.sender.send_message(
                            message.chat.id,
                            get_translation_for(user.language, 'message_type_not_supported_msg')
                        )
            except:
                logger.exception("Error!")

        @self.bot.callback_query_handler(func=lambda call: True)
        def callback_inline(reply: CallbackQuery):
            try:
                user = self.__get_or_register_user(reply.from_user.id, reply.from_user)
                if user is None or blocked_check(reply.message.chat, user):
                    return
                if 'to_state' in reply.data:
                    data = reply.data.split(";")
                    state_name = data[1]
                    self.__send_user_to_state(reply.message, user, state_name)
                    self.sender.answer_callback_query(reply.id)
                else:
                    self.__process_callback(reply, user)
            except:
                logger.exception("Error!")

    def __register_state(self, state_class):
        self.__states[state_class.__name__] = state_class()

    def __register_states(self, *states):
        for state in states:
            self.__register_state(state)

    def __get_state(self, name):
        if callable(name):
            name = name.__name__
        ret = self.__states.get(name)
        if ret is None:
            logger.error('No state with name "{}"'.format(name))
            return False, self.__states[self.__start_state], self.__start_state
        return True, ret, name

    def __process_ret_code(self, ret_tuple):
        """
        :param ret_tuple:
            ret_tuple[0] - ret code
            ret_tuple[1] - ret value
            ret_tuple[2] - message, callback or None
            ret_tuple[3] - user or None
        :return:
        """
        if ret_tuple[0] == base_state.RET_OK:
            return

        if ret_tuple[0] == base_state.RET_GO_TO_STATE:
            self.__send_user_to_state(ret_tuple[2], ret_tuple[3], ret_tuple[1])
        elif ret_tuple[0] == base_state.RET_ANSWER_CALLBACK:
            self.bot.answer_callback_query(ret_tuple[2].id)
        elif ret_tuple[0] == base_state.RET_ANSWER_AND_GO_TO_STATE:
            self.bot.answer_callback_query(ret_tuple[2].id)
            self.__send_user_to_state(ret_tuple[2].message, ret_tuple[3], ret_tuple[1])

    def __process_message(self, message, user):
        state_correct, current_state, c_name = self.__get_state(user_last_state(user, self.__start_state))
        if not state_correct:
            State(name=c_name, user=user).save()
        try:
            self.__process_ret_code(current_state.process_message_with_buttons(message, user, self.sender))
        except:
            logger.exception(self.__get_state(user_last_state(user, 'Unknown state')))

    def __process_callback(self, callback, user):
        state_correct, current_state, c_name = self.__get_state(user_last_state(user, self.__start_state))
        if not state_correct:
            State(name=c_name, user=user).save()
        try:
            self.__process_ret_code(current_state.process_callback(callback, user, self.sender))
        except:
            logger.exception(self.__get_state(user_last_state(user, 'Unknown state')))

    def __send_user_to_state(self, message, user, state):
        state_correct, current_state, c_name = self.__get_state(state)
        State(name=c_name, user=user).save()
        user_states = State.objects.filter(user=user)
        if user_states.count() > STATES_HISTORY_LEN:
            user_states.first().delete()
        try:
            if current_state.has_entry:
                self.__process_ret_code(current_state.entry(message, user, self.sender))
            else:
                self.__process_ret_code(current_state.process_message_with_buttons(message, user, self.sender))
        except:
            logger.exception(self.__get_state(user_last_state(user, 'Unknown state')))

    def run(self):
        # create_translation()
        self.bot.remove_webhook()
        self.bot.polling(none_stop=True)


if DEVELOP:
    from django_scaffold.settings import BOT_TOKEN

bot_handler = BotHandlers(BOT_TOKEN) if DEVELOP else None
