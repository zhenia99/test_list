from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    open_tag = 0
    close_tag = 0
    correct = True
    error_msg = ''
    allow_tags = ['b', 'i', 'a', 'code', 'pre']

    def handle_starttag(self, tag, attrs):
        if str(tag) in self.allow_tags:
            if self.open_tag == self.close_tag:
                self.open_tag += 1
            else:
                self.error_msg = 'Нельзя использовать вложенные html теги'
                self.correct = False
        else:
            self.error_msg = 'Вы используете не правильный html. Возможно тег не поддерживается телеграмом.' \
                             'Список разрешенных тегов: {}'.format(', '.join(self.allow_tags))
            self.correct = False

    def handle_endtag(self, tag):
        self.close_tag += 1

    def feed(self, data):
        self.open_tag = 0
        self.close_tag = 0
        self.correct = True
        self.error_msg = ''
        HTMLParser.feed(self, data)
        if self.correct:
            if not self.open_tag == self.close_tag:
                self.error_msg = 'Вы не закрыли html тег'
                self.correct = False
        return self.correct, self.error_msg
