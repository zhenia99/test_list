from telebot.apihelper import ApiException
from django_scaffold.settings import ADMIN_CHAT
from main.models import FeedbackMsg, FeedbackFromAdminMsg
from main.logger_settings import logger
from main.translations import get_translation_for


def answer_from_admin_chat(message, sender):
    try:
        if message.reply_to_message.from_user.id == sender.get_me().id:
            feedback_msg = FeedbackMsg.objects.filter(msg_id_admin=message.reply_to_message.message_id).select_related(

            ).first()
            if feedback_msg:
                message_send = None
                if message.content_type == 'text':
                    try:
                        message_send = sender.send_message(feedback_msg.user.user_id_int,
                                                           message.text,
                                                           reply_to_message_id=feedback_msg.msg_id)
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_message(feedback_msg.user.user_id_int,
                                                               get_templ_for_admin_answer(message.text,
                                                                                          feedback_msg.user.language))
                        else:
                            logger.exception(e)
                            return

                elif message.content_type == 'photo':
                    try:
                        message_send = sender.send_photo(feedback_msg.user.user_id_int,
                                                         message.photo[0].file_id, caption=message.caption,
                                                         reply_to_message_id=feedback_msg.msg_id, parse_mode='html')
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_photo(feedback_msg.user.user_id_int,
                                                             message.photo[0].file_id,
                                                             caption=get_templ_for_admin_answer(
                                                                 message.caption,
                                                                 feedback_msg.user.language),
                                                             parse_mode='html')
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'audio':
                    try:
                        message_send = sender.send_audio(feedback_msg.user.user_id_int,
                                                         message.audio.file_id, caption=message.caption,
                                                         reply_to_message_id=feedback_msg.msg_id, parse_mode='html')
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_audio(feedback_msg.user.user_id_int,
                                                             message.audio.file_id, caption=get_templ_for_admin_answer(
                                    message.caption,
                                    feedback_msg.user.language),
                                                             parse_mode='html')
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'document':
                    try:
                        message_send = sender.send_document(feedback_msg.user.user_id_int,
                                                            message.document.file_id, caption=message.caption,
                                                            reply_to_message_id=feedback_msg.msg_id, parse_mode='html')
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_document(feedback_msg.user.user_id_int,
                                                                message.document.file_id,
                                                                caption=get_templ_for_admin_answer(
                                                                    message.caption,
                                                                    feedback_msg.user.language),
                                                                parse_mode='html')
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'video':
                    try:
                        message_send = sender.send_video(feedback_msg.user.user_id_int,
                                                         message.video.file_id, caption=message.caption,
                                                         reply_to_message_id=feedback_msg.msg_id, parse_mode='html')
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_video(feedback_msg.user.user_id_int,
                                                             message.video.file_id,
                                                             caption=get_templ_for_admin_answer(
                                                                 message.caption,
                                                                 feedback_msg.user.language),
                                                             parse_mode='html')
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'video_note':
                    try:
                        message_send = sender.send_video_note(feedback_msg.user.user_id_int,
                                                              message.video_note.file_id,
                                                              reply_to_message_id=feedback_msg.msg_id)
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_video_note(feedback_msg.user.user_id_int,
                                                                  message.video_note.file_id)
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'voice':
                    try:
                        message_send = sender.send_voice(feedback_msg.user.user_id_int,
                                                         message.voice.file_id,
                                                         reply_to_message_id=feedback_msg.msg_id)
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_voice(feedback_msg.user.user_id_int,
                                                             message.voice.file_id)
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'contact':
                    try:
                        message_send = sender.send_contact(feedback_msg.user.user_id_int,
                                                           reply_to_message_id=feedback_msg.msg_id,
                                                           phone_number=message.contact.phone_number,
                                                           first_name=message.contact.first_name,
                                                           last_name=message.contact.last_name, )
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_contact(feedback_msg.user.user_id_int,
                                                               phone_number=message.contact.phone_number,
                                                               first_name=message.contact.first_name,
                                                               last_name=message.contact.last_name, )
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'location':
                    try:
                        message_send = sender.send_location(feedback_msg.user.user_id_int,
                                                            latitude=message.location.latitude,
                                                            longitude=message.location.longitude,
                                                            reply_to_message_id=feedback_msg.msg_id)
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_location(feedback_msg.user.user_id_int,
                                                                latitude=message.location.latitude,
                                                                longitude=message.location.longitude)
                        else:
                            logger.exception(e)
                            return
                elif message.content_type == 'venue':
                    try:
                        message_send = sender.send_venue(feedback_msg.user.user_id_int,
                                                         latitude=message.venue.location.latitude,
                                                         longitude=message.venue.location.longitude,
                                                         reply_to_message_id=feedback_msg.msg_id,
                                                         title=message.venue.title,
                                                         address=message.venue.address,
                                                         )
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            message_send = sender.send_venue(feedback_msg.user.user_id_int,
                                                             latitude=message.venue.location.latitude,
                                                             longitude=message.venue.location.longitude,
                                                             title=message.venue.title,
                                                             address=message.venue.address,
                                                             )
                        else:
                            logger.exception(e)
                            return
                if message_send:
                    register_answer_admin_msg(feedback_msg, message_send, message.message_id)
    except:
        logger.exception("Error!")


def get_templ_for_admin_answer(text, language='ru'):
    return get_translation_for(language, 'answer_from_admin_msg').format(text if text else '')


def register_answer_admin_msg(feedback_msg, message, msg_id_admin):
    feedback_msg = FeedbackFromAdminMsg(user=feedback_msg.user, msg_id=message.message_id, msg_id_admin=msg_id_admin)
    feedback_msg.save()


def answer_from_user_to_admin(user, message, sender):
    try:
        if message.reply_to_message:
            msg_from_admin = FeedbackFromAdminMsg.objects.filter(msg_id=message.reply_to_message.message_id,
                                                                 user__user_id_int=int(message.chat.id)).first()
            if msg_from_admin:
                if message.content_type == 'text':
                    try:
                        msg_to_admin = sender.send_message(ADMIN_CHAT,
                                                           message.text,
                                                           reply_to_message_id=msg_from_admin.msg_id_admin)
                    except ApiException as e:
                        if 'reply message not found' in str(e):
                            template_answer = get_translation_for('ru', 'template_for_admin_chat_from_user').format(
                                user.user_id_int,
                                user.username,
                                message.text
                            )
                            msg_to_admin = sender.send_message(ADMIN_CHAT,
                                                               template_answer, parse_mode='html')
                        else:
                            logger.exception(e)
                            return

                    feedback_msg = FeedbackMsg(user=user, msg_id=message.message_id,
                                               msg_id_admin=msg_to_admin.message_id)

                    feedback_msg.save()
                else:
                    sender.send_message(
                        message.chat.id,
                        get_translation_for(msg_from_admin.user.language, 'message_type_not_supported_msg')
                    )
    except:
        logger.exception("Error!")
