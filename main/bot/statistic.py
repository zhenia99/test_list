from main.models import Artifact, Click


def register_click(btn_name):
    click = Click.objects.filter(name_button=btn_name).first()
    if click is None:
        click = Click(
            name_button=btn_name,
            count=1
        )
        click.save()
    else:
        click.count += 1
        click.save()


def register_message(msg, user):
    artifact = Artifact.objects.filter(name=msg).first()
    if artifact is None:
        artifact = Artifact(name=msg, count=1)
        artifact.save()
        artifact.users.add(user)
    else:
        if user not in artifact.users.all():
            artifact.users.add(user)
        artifact.count += 1
        artifact.save()
