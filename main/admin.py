from django.contrib import admin

# Register your models here.

from main.models import State, User, ScheduledBroadCastSendTask, ScheduledBroadCastInlineButton


# @admin.register(Artifact)
# class ArtifactAdmin(admin.ModelAdmin):
#     pass
#
#
# @admin.register(Click)
# class ClickAdmin(admin.ModelAdmin):
#     pass

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'username_tg', 'first_name', 'email')


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'user')


@admin.register(ScheduledBroadCastSendTask)
class ScheduledBroadCastSendTaskAdmin(admin.ModelAdmin):
    list_display = ('pk', 'send_date', 'sent', 'file_type', 'message')


@admin.register(ScheduledBroadCastInlineButton)
class ScheduledBroadCastInlineButtonAdmin(admin.ModelAdmin):
    list_display = ('pk',)
