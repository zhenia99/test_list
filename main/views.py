from django.contrib.auth.views import LoginView
from django.shortcuts import redirect
from django.urls import reverse

from panel.forms import IndexLoginForm


def index(request):
    return redirect(reverse('panel:sender'))


class PanelLoginView(LoginView):
    form_class = IndexLoginForm
    template_name = 'panel/login.html'
