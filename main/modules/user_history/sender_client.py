import json
import pickle
import os
from base64 import b64encode
from requests import post, get
from telebot import TeleBot

from django_scaffold.settings import SENDER_HOST, SENDER_PORT, STORAGE_CHAT
from django.core.files import File as DjangoFile

SENDER_SEND_URL = '{}:{}/api/send_to_user'.format(SENDER_HOST, SENDER_PORT)
SENDER_STATUS_URL = '{}:{}/api/get_status'.format(SENDER_HOST, SENDER_PORT)
SENDER_STATISTIC_URL = '{}:{}/api/get_statistic'.format(SENDER_HOST, SENDER_PORT)
SENDER_RESPONSE_TASK_URL = '{}:{}/api/get_response_by_task_id'.format(SENDER_HOST, SENDER_PORT)
SENDER_EDIT_TASK_URL = '{}:{}/api/edit_task'.format(SENDER_HOST, SENDER_PORT)
SENDER_REMOVE_TASK_URL = '{}:{}/api/remove_task'.format(SENDER_HOST, SENDER_PORT)
SENDER_EDIT_MESSAGE_TEXT_URL = '{}:{}/api/edit_message_text'.format(SENDER_HOST, SENDER_PORT)


class SenderException(Exception):
    pass


class SenderClient(object):

    def __init__(self, token):
        self.__token = token
        self.__last_task = None
        self.__temp_bot = TeleBot(self.__token, threaded = False)

    def get_bot(self):
        return self.__temp_bot

    def __make_request(self, data, files=None):
        data['token'] = self.__token
        r = post(SENDER_SEND_URL, data=data, files=files)
        if r.status_code != 200:
            self.__last_task = None
            raise SenderException(r.text)
        self.__last_task = r.text
        return r

    def __send_data(self, targets, data, document_type, caption=None, reply_markup=None, parse_mode=None, priority=None, broadcast=None):
        if not isinstance(targets, list):
            targets = [targets,]
        payload = {
            'message': caption,
            'targets': json.dumps(targets),
            'reply_markup': b64encode(pickle.dumps(reply_markup, 4)),
            'parse_mode': parse_mode,
            'document_type': document_type,
            'priority': priority,
            'send_to_all': broadcast,
        }
        files = None
        if isinstance(data, str):
            payload['document_id'] = data
        else:
            files = {
                'document': data
            }
        return self.__make_request(payload, files=files)

    def savemessage(self, text, targets):
        from main.models import Message
        for target in targets:
            Message.objects.create(
                user_id = target,
                message = text,
                from_user = False
            )

    def send_message(self, targets, text, reply_markup=None, parse_mode='html', disable_web_page_preview=False,
                     priority=1, broadcast=None):
        if not isinstance(targets, list):
            targets = [targets, ]
        payload = {
            'message': text,
            'targets': json.dumps(targets),
            'reply_markup': b64encode(pickle.dumps(reply_markup, 4)),
            'parse_mode': parse_mode,
            'disable_web_page_preview': disable_web_page_preview,
            'priority': priority,
            'send_to_all': broadcast,
        }
        self.savemessage(text, targets)
        return self.__make_request(payload)

    def send_document(self, targets, data, caption=None, reply_markup=None, parse_mode=None, priority=1, broadcast=None):
        from main.models import Message, File
        
        file_id = data
        if not File.objects.filter(file_id=file_id).exists():
            tg_file = self.__temp_bot.get_file(file_id)
            ext = tg_file.file_path.split('.')[-1]
            downloaded_file = self.__temp_bot.download_file(tg_file.file_path)
            
            with open(f'{file_id}.{ext}','wb') as new_file:
                new_file.write(downloaded_file)
                
            local_file = open(f'{file_id}.{ext}', 'rb')
            djangofile = DjangoFile(local_file)
            
            savedfile = File()
            savedfile.file_id = file_id
            savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
            
            os.remove(f'{file_id}.{ext}')
                
        for target in targets:
            msg = Message.objects.create(
                user_id = target,
                message = caption,
                from_user = False, 
                file_id = file_id,
                filetype = 'document'
            )
        return self.__send_data(
            targets, data, 'document', caption=caption, reply_markup=reply_markup, parse_mode=parse_mode, priority=priority, broadcast=broadcast
        )
        
    def savephoto(self, data, caption, targets):
        from main.models import Message as MessageModel, File

        if isinstance(data, str):
            file_id = data
            if not File.objects.filter(file_id=file_id).exists():
                tg_file = self.__temp_bot.get_file(file_id)
                ext = tg_file.file_path.split('.')[-1]
                downloaded_file = self.__temp_bot.download_file(tg_file.file_path)
                
                with open(f'{file_id}.{ext}','wb') as new_file:
                    new_file.write(downloaded_file)
                    
                local_file = open(f'{file_id}.{ext}', 'rb')
                djangofile = DjangoFile(local_file)
                
                savedfile = File()
                savedfile.file_id = file_id
                savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                
                os.remove(f'{file_id}.{ext}')
                    
            for target in targets:
                msg = MessageModel.objects.create(
                    user_id = target,
                    message = caption,
                    from_user = False, 
                    file_id = file_id,
                    filetype = 'photo'
                )

    def send_photo(self, targets, data, caption=None, reply_markup=None, parse_mode=None, priority=1, broadcast=None):
        self.savephoto(data, caption, targets)
        return self.__send_data(
            targets, data, 'photo', caption=caption, reply_markup=reply_markup, parse_mode=parse_mode, priority=priority, broadcast=broadcast
        )

    def send_video(self, targets, data, caption=None, reply_markup=None, parse_mode=None, priority=1, broadcast=None):
        from main.models import Message
        for target in targets:
            Message.objects.create(
                user_id = target,
                message = caption,
                from_user = False,
                filetype = 'video'
            )
        return self.__send_data(
            targets, data, 'video', caption=caption, reply_markup=reply_markup, parse_mode=parse_mode, priority=priority, broadcast=broadcast
        )

    def send_video_note(self, targets, data, priority=1, broadcast=None):
        from main.models import Message
        for target in targets:
            Message.objects.create(
                user_id = target,
                from_user = False,
                filetype = 'video_note'
            )
        return self.__send_data(
            targets, data, 'video_note', priority=priority, broadcast=broadcast
        )

    def send_audio(self, targets, data, caption=None, reply_markup=None, parse_mode=None, priority=1, broadcast=None):
        from main.models import Message
        for target in targets:
            Message.objects.create(
                user_id = target,
                message = caption,
                from_user = False,
                filetype = 'audio'
            )
        return self.__send_data(
            targets, data, 'audio', caption=caption, reply_markup=reply_markup, parse_mode=parse_mode, priority=priority, broadcast=broadcast
        )

    def send_voice(self, targets, data, caption=None, reply_markup=None, parse_mode=None, priority=1, broadcast=None):
        from main.models import Message, File
        
        file_id = data
        if not File.objects.filter(file_id=file_id).exists():
            tg_file = self.__temp_bot.get_file(file_id)
            ext = tg_file.file_path.split('.')[-1]
            downloaded_file = self.__temp_bot.download_file(tg_file.file_path)
            
            with open(f'{file_id}.{ext}','wb') as new_file:
                new_file.write(downloaded_file)
                
            local_file = open(f'{file_id}.{ext}', 'rb')
            djangofile = DjangoFile(local_file)
            
            savedfile = File()
            savedfile.file_id = file_id
            savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
            
            os.remove(f'{file_id}.{ext}')
        
        for target in targets:
            Message.objects.create(
                user_id = target,
                message = caption,
                from_user = False,
                filetype = 'voice',
                file_id = file_id
            )
        return self.__send_data(
            targets, data, 'voice', caption=caption, reply_markup=reply_markup, parse_mode=parse_mode, priority=priority, broadcast=broadcast
        )

    def send_sticker(self, targets, data, caption=None, reply_markup=None, priority=1, broadcast=None):
        from main.models import Message
        for target in targets:
            Message.objects.create(
                user_id = target,
                message = text,
                from_user = False,
                filetype = 'sticker'
            )
        return self.__send_data(
            targets, data, 'sticker', reply_markup=reply_markup, priority=priority, broadcast=broadcast
        )

    def get_status(self):
        payload = {
            'bot_id': self.__token
        }
        r = get(SENDER_STATUS_URL, params=payload)
        if r.status_code != 200:
            raise SenderException(r.text)
        return json.loads(r.text)

    def answer_callback_query(self, callback_query_id, text=None, show_alert=None, url=None, cache_time=None):
        return self.__temp_bot.answer_callback_query(callback_query_id, text, show_alert, url, cache_time)

    def edit_message_reply_markup(self, chat_id=None, message_id=None, inline_message_id=None, reply_markup=None):
        return self.__temp_bot.edit_message_reply_markup(chat_id, message_id, inline_message_id, reply_markup)

    def edit_message_text(self, text, chat_id, message_id, inline_message_id=None, reply_markup=None,
                          parse_mode='html', disable_web_page_preview=False):
        payload = {
            'token': self.__token,
            'reply_markup': b64encode(pickle.dumps(reply_markup, 4)),
            'text': text,
            'chat_id': chat_id,
            'message_id': message_id,
            'inline_message_id': inline_message_id,
            'parse_mode': parse_mode,
            'disable_web_page_preview': disable_web_page_preview,
        }
        r = post(SENDER_EDIT_MESSAGE_TEXT_URL, payload)
        return r.text

    def get_blocked_stats(self):
        payload = {
            'bot_id': self.__token
        }
        r = get(SENDER_STATISTIC_URL, params=payload)
        if r.status_code != 200:
            raise SenderException(r.text)
        return json.loads(r.text)

    def get_task_response(self, task_id):
        try:
            kwargs = {
                'task_id': task_id,
            }
            response = get(SENDER_RESPONSE_TASK_URL, params=kwargs)
            if response.status_code != 200:
                raise SenderException(response.text)
            return json.loads(response.text)
        except SenderException:
            return None

    def edit_broadcast(self, task_id, message, is_caption):
        kwargs = {
            'token': self.__token,
            'task_id': task_id,
            'message': message,
            'is_caption': is_caption
        }
        response = post(SENDER_EDIT_TASK_URL, data=kwargs)
        if response.status_code != 200:
            raise SenderException(response.text)
        return response.text

    def remove_broadcast(self, task_id):
        kwargs = {
            'token': self.__token,
            'task_id': task_id
        }
        response = post(SENDER_REMOVE_TASK_URL, data=kwargs)
        if response.status_code != 200:
            raise SenderException(response.text)
        return response.text
