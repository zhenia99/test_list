import telebot
from django_scaffold.settings import BOT_TOKEN

bot = telebot.TeleBot(BOT_TOKEN, threaded=False)

def savemessage(text, target):
    from main.models import Message, File
    
    Message.objects.create(
        user_id = target,
        message = text,
        from_user = False
    )

def overload_sendMessage():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request

    def send_message(token, chat_id, text, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None, parse_mode=None, disable_notification=None, timeout=None):
        method_url = r'sendMessage'
        payload = {'chat_id': str(chat_id), 'text': text}
        if disable_web_page_preview is not None:
            payload['disable_web_page_preview'] = disable_web_page_preview
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if parse_mode:
            payload['parse_mode'] = parse_mode
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if timeout:
            payload['connect-timeout'] = timeout
        msg = _make_request(token, method_url, params=payload, method='post')
        if int(chat_id) > 0:
            savemessage(text, chat_id)
        return msg

    telebot.apihelper.send_message = send_message
    

def savephoto(data, caption, target):
    import telebot
    from telebot.apihelper import _convert_markup, _make_request
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os

    if isinstance(data, str):
        file_id = data
        if not File.objects.filter(file_id=file_id).exists():
            tg_file = bot.get_file(file_id)
            ext = tg_file.file_path.split('.')[-1]
            downloaded_file = bot.download_file(tg_file.file_path)
            
            with open(f'{file_id}.{ext}','wb') as new_file:
                new_file.write(downloaded_file)
                
            local_file = open(f'{file_id}.{ext}', 'rb')
            djangofile = DjangoFile(local_file)
            
            savedfile = File()
            savedfile.file_id = file_id
            savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
            
            os.remove(f'{file_id}.{ext}')
                
        Message.objects.create(
            user_id = target,
            message = caption,
            from_user = False, 
            file_id = file_id,
            filetype = 'photo'
        )


def overload_sendPhoto():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os
    
    def send_photo(token, chat_id, photo, caption=None, reply_to_message_id=None, reply_markup=None, parse_mode=None, disable_notification=None, timeout=None):
        method_url = r'sendPhoto'
        payload = {'chat_id': chat_id}
        files = None
        if util.is_string(photo):
            payload['photo'] = photo
        else:
            files = {'photo': photo}
        if caption:
            payload['caption'] = caption
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if parse_mode:
            payload['parse_mode'] = parse_mode
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if timeout:
            payload['connect-timeout'] = timeout
        msg = _make_request(token, method_url, params=payload, files=files, method='post')
        if int(chat_id) > 0:
            file_id = msg['photo'][-1]['file_id']
            savephoto(file_id, caption, chat_id)
            
        return msg
    
    telebot.apihelper.send_photo = send_photo
    
    
def overload_sendLocation():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os
    
    def send_location(
        token, chat_id, latitude, longitude,
        live_period=None, reply_to_message_id=None, reply_markup=None,
        disable_notification=None, timeout=None):
        method_url = r'sendLocation'
        payload = {'chat_id': chat_id, 'latitude': latitude, 'longitude': longitude}
        if live_period:
            payload['live_period'] = live_period
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if timeout:
            payload['connect-timeout'] = timeout
        msg = _make_request(token, method_url, params=payload)
        if int(chat_id) > 0:
            Message.objects.create(
                user_id = chat_id,
                from_user = False, 
                filetype = 'location'
            )
        
        return msg
    
    telebot.apihelper.send_location = send_location
    
    
def overload_sendContact():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os
    
    def send_contact(
            token, chat_id, phone_number, first_name,
            last_name=None, disable_notification=None,
            reply_to_message_id=None, reply_markup=None, timeout=None):
        method_url = r'sendContact'
        payload = {'chat_id': chat_id, 'phone_number': phone_number, 'first_name': first_name}
        if last_name:
            payload['last_name'] = last_name
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if timeout:
            payload['connect-timeout'] = timeout
        msg = _make_request(token, method_url, params=payload)
        if int(chat_id) > 0:
            Message.objects.create(
                user_id = chat_id,
                from_user = False, 
                filetype = 'contact'
            )
        
        return msg
    
    telebot.apihelper.send_contact = send_contact
    
    
def overload_sendVideo():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os
    
    def send_video(token, chat_id, data, duration=None, caption=None, reply_to_message_id=None, reply_markup=None,
               parse_mode=None, supports_streaming=None, disable_notification=None, timeout=None, thumb=None):
        method_url = r'sendVideo'
        payload = {'chat_id': chat_id}
        files = None
        if not util.is_string(data):
            files = {'video': data}
        else:
            payload['video'] = data
        if duration:
            payload['duration'] = duration
        if caption:
            payload['caption'] = caption
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if parse_mode:
            payload['parse_mode'] = parse_mode
        if supports_streaming is not None:
            payload['supports_streaming'] = supports_streaming
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if timeout:
            payload['connect-timeout'] = timeout
        if thumb:
            if not util.is_string(thumb):
                files['thumb'] = thumb
            else:
                payload['thumb'] = thumb
        msg = _make_request(token, method_url, params=payload, files=files, method='post')
        if int(chat_id) > 0:
            file_id = msg['video']['file_id']
            if not File.objects.filter(file_id=file_id).exists():
                tg_file = bot.get_file(file_id)
                ext = tg_file.file_path.split('.')[-1]
                downloaded_file = bot.download_file(tg_file.file_path)
                
                with open(f'{file_id}.{ext}','wb') as new_file:
                    new_file.write(downloaded_file)
                    
                local_file = open(f'{file_id}.{ext}', 'rb')
                djangofile = DjangoFile(local_file)
                
                savedfile = File()
                savedfile.file_id = file_id
                savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                
                os.remove(f'{file_id}.{ext}')

            Message.objects.create(
                user_id = chat_id,
                from_user = False, 
                filetype = 'video',
                file_id = file_id
            )
        
        return msg
    
    telebot.apihelper.send_video = send_video
    
    
def overload_sendAudio():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os
    
    def send_audio(token, chat_id, audio, caption=None, duration=None, performer=None, title=None, reply_to_message_id=None,
               reply_markup=None, parse_mode=None, disable_notification=None, timeout=None, thumb=None):
        method_url = r'sendAudio'
        payload = {'chat_id': chat_id}
        files = None
        if not util.is_string(audio):
            files = {'audio': audio}
        else:
            payload['audio'] = audio
        if caption:
            payload['caption'] = caption
        if duration:
            payload['duration'] = duration
        if performer:
            payload['performer'] = performer
        if title:
            payload['title'] = title
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if parse_mode:
            payload['parse_mode'] = parse_mode
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if timeout:
            payload['connect-timeout'] = timeout
        if thumb:
            if not util.is_string(thumb):
                files['thumb'] = thumb
            else:
                payload['thumb'] = thumb
        msg = _make_request(token, method_url, params=payload, files=files, method='post')
        
        if int(chat_id) > 0:
            Message.objects.create(
                user_id = chat_id,
                from_user = False, 
                filetype = 'audio'
            )
        
        return msg
    
    telebot.apihelper.send_audio = send_audio
    
    
def overload_sendData():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request, get_method_by_type
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os
    
    def send_data(token, chat_id, data, data_type, reply_to_message_id=None, reply_markup=None, parse_mode=None,
              disable_notification=None, timeout=None, caption=None):
        method_url = get_method_by_type(data_type)
        payload = {'chat_id': chat_id}
        files = None
        if not util.is_string(data):
            files = {data_type: data}
        else:
            payload[data_type] = data
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if parse_mode and data_type == 'document':
            payload['parse_mode'] = parse_mode
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if timeout:
            payload['connect-timeout'] = timeout
        if caption:
            payload['caption'] = caption
        msg = _make_request(token, method_url, params=payload, files=files, method='post')
        
        if int(chat_id) > 0:
            file_id = None
            if 'document' in msg.keys():
                file_id = msg['document']['file_id']
            if 'audio' in msg.keys():
                file_id = msg['audio']['file_id']
            if file_id:
                if not File.objects.filter(file_id=file_id).exists():
                    tg_file = bot.get_file(file_id)
                    ext = tg_file.file_path.split('.')[-1]
                    downloaded_file = bot.download_file(tg_file.file_path)
                    
                    with open(f'{file_id}.{ext}','wb') as new_file:
                        new_file.write(downloaded_file)
                        
                    local_file = open(f'{file_id}.{ext}', 'rb')
                    djangofile = DjangoFile(local_file)
                    
                    savedfile = File()
                    savedfile.file_id = file_id
                    savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                    
                    os.remove(f'{file_id}.{ext}')
                        
                Message.objects.create(
                    user_id = int(chat_id),
                    message = caption,
                    from_user = False, 
                    file_id = file_id,
                    filetype = 'document'
                )
        
        return msg
    
    telebot.apihelper.send_data = send_data
    

def overload_sendVoice():
    import telebot
    from telebot.apihelper import _convert_markup, _make_request
    from telebot import util
    from main.models import Message, File
    from django.core.files import File as DjangoFile
    import os
    
    def send_voice(token, chat_id, voice, caption=None, duration=None, reply_to_message_id=None, reply_markup=None,
               parse_mode=None, disable_notification=None, timeout=None):
        method_url = r'sendVoice'
        payload = {'chat_id': chat_id}
        files = None
        if not util.is_string(voice):
            files = {'voice': voice}
        else:
            payload['voice'] = voice
        if caption:
            payload['caption'] = caption
        if duration:
            payload['duration'] = duration
        if reply_to_message_id:
            payload['reply_to_message_id'] = reply_to_message_id
        if reply_markup:
            payload['reply_markup'] = _convert_markup(reply_markup)
        if parse_mode:
            payload['parse_mode'] = parse_mode
        if disable_notification is not None:
            payload['disable_notification'] = disable_notification
        if timeout:
            payload['connect-timeout'] = timeout
        msg = _make_request(token, method_url, params=payload, files=files, method='post')
        if int(chat_id) > 0:
            file_id = msg['voice']['file_id']
            if not File.objects.filter(file_id=file_id).exists():
                tg_file = bot.get_file(file_id)
                ext = tg_file.file_path.split('.')[-1]
                downloaded_file = bot.download_file(tg_file.file_path)
                
                with open(f'{file_id}.{ext}','wb') as new_file:
                    new_file.write(downloaded_file)
                    
                local_file = open(f'{file_id}.{ext}', 'rb')
                djangofile = DjangoFile(local_file)
                
                savedfile = File()
                savedfile.file_id = file_id
                savedfile.file_obj.save(f'{file_id}.{ext}', djangofile, save=True)
                
                os.remove(f'{file_id}.{ext}')

            Message.objects.create(
                user_id = chat_id,
                from_user = False, 
                filetype = 'voice',
                file_id = file_id,
            )
        
        return msg
        
    telebot.apihelper.send_voice = send_voice