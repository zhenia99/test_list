from threading import Thread
from time import sleep

from django.apps import AppConfig

from main.logger_settings import logger


class MainConfig(AppConfig):
    name = 'main'

    def ready(self):
        from django.db.migrations.executor import MigrationExecutor
        from django.db import connections, DEFAULT_DB_ALIAS
        
        def overloads():
            from main.modules.user_history.overloads import overload_sendMessage, overload_sendPhoto, overload_sendLocation, overload_sendContact, overload_sendVideo, overload_sendAudio, overload_sendData, overload_sendVoice
            overload_sendMessage()
            overload_sendPhoto()
            overload_sendLocation()
            overload_sendContact()
            overload_sendVideo()
            overload_sendAudio()
            overload_sendData()
            overload_sendVoice()

        def is_database_synchronized(database):
            connection = connections[database]
            connection.prepare_database()
            executor = MigrationExecutor(connection)
            targets = executor.loader.graph.leaf_nodes()
            return False if executor.migration_plan(targets) else True

        if is_database_synchronized(DEFAULT_DB_ALIAS):
            from django_scaffold.settings import BOT_TOKEN, DEVELOP, SITE_URL, INSTALLED_MODULES
            from main.bot.handler import BotHandlers
            from main.translations import create_translation
            from main.bot.scheduler import run_scheduler
            from main.bot.utils import cache_stats
            if INSTALLED_MODULES.get('user_history'):
                overloads()
            bot = BotHandlers(BOT_TOKEN)
            bot.bot.remove_webhook()
            print(bot.bot.get_me().username)
            scheduler_thread = Thread(target=run_scheduler)
            scheduler_thread.start()
            cache_stats()
            if not DEVELOP:
                response = bot.bot.set_webhook(SITE_URL + 'web_hook/{}'.format(BOT_TOKEN),
                                               certificate=open(
                                                   '/root/english_test_bot/vassals/bot/certs/webhook_cert.pem', 'r')
                                               )
                print(response)
            else:
                polling = Thread(target=bot.run)
                polling.start()
            create_translation()
