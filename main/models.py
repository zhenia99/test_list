import uuid

import pytz
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone

from django_scaffold.settings import BOT_TOKEN

from django_scaffold.settings import INSTALLED_MODULES
if INSTALLED_MODULES.get('user_history'):
    from main.modules.user_history.sender_client import SenderClient
else:
    from main.bot.sender_client import SenderClient

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
)

STATES = (
    ('ChooseLanguageState', 'Выбор Языка'),
    ('MenuState', 'Меню'),
)

FILE_TYPES = (
    ('audio', 'Аудио'),
    ('image', 'Изображение'),
    ('video', 'Видео'),
    ('document', 'Документ'),
)


class SystemLanguage(models.Model):
    language_code = models.CharField(max_length=2, unique=True, verbose_name='Код языка')

    def __str__(self):
        return self.language_code


class User(AbstractUser):
    user_id_int = models.IntegerField(null=True, blank=True)
    username_tg = models.CharField(max_length=1024, null=True, blank=True)
    first_name = models.CharField(max_length=150, null=True, blank=True)
    user_id_s = models.CharField(max_length=256, null=True, blank=True)
    language = models.CharField(max_length=2, default='ru')
    is_blocked = models.BooleanField(default=False, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    name = models.CharField(max_length=256, null=True, blank=True)
    update_date = models.DateTimeField(verbose_name='время обновления юзера ботом', auto_now=True)

    def __str__(self):
        return '{} - {}'.format(self.username_tg, self.update_date)


class Project(models.Model):
    #user = models.ManyToManyField(User)
    date_hosting = models.CharField(max_length=20)
    date_domain = models.CharField(max_length=20)

    def __str__(self):
        return 'Hosting: {}; Domain: {}'.format(self.date_hosting, self.date_domain)        


class TokenLanguageString(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class LanguageString(models.Model):
    language_code = models.ForeignKey(SystemLanguage, on_delete=models.CASCADE)
    token = models.ForeignKey(TokenLanguageString, on_delete=models.CASCADE)
    string = models.TextField()

    def __str__(self):
        return '{} -> {} -> {}'.format(self.language_code, self.token, self.string)


class Artifact(models.Model):
    name = models.CharField(max_length=4096)
    count = models.IntegerField(default=0)
    is_view = models.BooleanField(default=True)
    users = models.ManyToManyField('User')

    @property
    def users_exists(self):
        return self.users.all().exists()


class Click(models.Model):
    name_button = models.CharField(max_length=255)
    count = models.IntegerField(default=0)


class State(models.Model):
    name = models.CharField(max_length=256)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class StatisticLanguageDay(models.Model):
    date = models.DateField()
    language = models.CharField(max_length=10, default='ru')
    count = models.IntegerField(default=1)


class StatisticUser(models.Model):
    date = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_language = models.CharField(max_length=10, default='ru')
    cached = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.cached = True
        super().save(*args, **kwargs)
        statscounter = StatisticLanguageDay.objects.filter(date=self.date, language=self.user_language).first()
        if not statscounter:
            StatisticLanguageDay.objects.create(date=self.date, language=self.user_language)
        else:
            statscounter.count += 1
            statscounter.save()


class BroadCastSendTask(models.Model):
    task_id = models.CharField(max_length=300, verbose_name='celery task id')
    message = models.CharField(max_length=4096, verbose_name='message text', null=True, blank=True)


class ScheduledBroadCastSendTask(models.Model):
    message = models.CharField(max_length=4096, verbose_name='message text', null=True, blank=True)
    file_to_send_id = models.CharField(max_length=512, null=True, blank=True)
    file_type = models.CharField(choices=FILE_TYPES, verbose_name='Тип файла', max_length=64, null=True, blank=True)
    send_date = models.DateTimeField(verbose_name='Дата отправки рассылки')
    targets = models.ManyToManyField('User', related_name='Scheduled_task')
    sent = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def date_formatted(self):
        kiev_tz = pytz.timezone('Europe/Kiev')
        return self.send_date.astimezone(kiev_tz).\
            strftime('%d %B %Y, %H:%M').replace('January', 'Января').replace('February', 'Февраля').\
            replace('March', 'Марта').replace('April', 'Апреля').replace('May', 'Мая').replace('June', 'Июня').\
            replace('July', 'Июля').replace('August', 'Августа').replace('September', 'Сентября').\
            replace('October', 'Октября').replace('November', 'Ноября').replace('December', 'Декабря')

    @property
    def targets_count(self):
        return self.targets.exclude(is_superuser=True).count()

    @property
    def have_keyboard(self) -> bool:
        return ScheduledBroadCastInlineButton.objects.filter(scheduled_task=self).exists()

    @property
    def keyboard_width(self) -> int:
        return max([button.column for button in ScheduledBroadCastInlineButton.objects.filter(scheduled_task=self)])


class ScheduledBroadCastInlineButton(models.Model):
    scheduled_task = models.ForeignKey('ScheduledBroadCastSendTask', verbose_name='Рассылка',  on_delete=models.CASCADE)
    text = models.CharField(max_length=128, verbose_name='Текст')
    url = models.CharField(max_length=512, verbose_name='Ссылка', blank=True, null=True)
    state = models.CharField(max_length=128, verbose_name='Стейт', blank=True, null=True)
    to_state_enabled = models.BooleanField(default=False)
    row = models.IntegerField(verbose_name='Ряд', default=1)
    column = models.IntegerField(verbose_name='Позиция', default=1)


class FeedbackMsg(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    msg_id = models.IntegerField(blank=True, null=True)
    msg_id_admin = models.IntegerField()


class FeedbackFromAdminMsg(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    msg_id = models.IntegerField()
    msg_id_admin = models.IntegerField()


class StateLink(models.Model):
    state_link_id = models.CharField(max_length=255, default=str(uuid.uuid4().hex), unique=True)
    state = models.CharField(verbose_name='Название стейта', max_length=1000, choices=STATES)
    link_id = models.CharField(max_length=1000, verbose_name='Ссылка на стейт')
    state_name = models.CharField(max_length=1000, null=True, blank=True)
    tag = models.CharField(verbose_name='Тег', max_length=1024)
    statistic_count = models.IntegerField(default=0)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.pk:
            self.state_link_id = str(uuid.uuid4().hex)
        link_id = f'https://t.me/{SenderClient(BOT_TOKEN).get_bot().get_me().username}?start={self.state_link_id}'
        self.link_id = link_id
        for i in STATES:
            if i[0] == self.state:
                self.state_name = i[1]
        super().save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return str(self.pk)


class UsersStateLinks(models.Model):
    selected_state_link = models.ForeignKey('StateLink', on_delete=models.SET_NULL, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    
class File(models.Model):
    file_id = models.CharField(max_length=256)
    file_obj = models.FileField(upload_to='saved_media')
    
    def __str__(self):
        return self.file_id


class Message(models.Model):
    user_id = models.IntegerField(null=True, blank=True, verbose_name='Телеграмм ID')
    time = models.DateTimeField(auto_now_add=True, null=True)
    message = models.CharField(max_length=40964, null=True)
    from_user = models.BooleanField()
    filetype = models.CharField(max_length=64, null=True)
    file_id = models.CharField(max_length=256, null=True)
    
    @property
    def date(self):
        return self.time

    @property
    def user(self):
        return User.objects.get(user_id_int=self.user_id).first_name if self.from_user else "Bot"
    
    @property
    def initial(self):
        return User.objects.get(user_id_int=self.user_id).first_name[:2] if self.from_user else "B"
    
    @property
    def userpic(self):
        return 'userpic2' if self.from_user else 'userpic1'

    @property
    def text(self):
        return self.message
    
    @property
    def newblock(self):
        msg = Message.objects.filter(time__lt = self.time, user_id=self.user_id).order_by('-time').first()
        if msg:
            return not self.user == msg.user
        return True
    
    @property
    def fileurl(self):
        f = File.objects.filter(file_id=self.file_id).first()
        return f.file_obj.url
