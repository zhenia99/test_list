import uuid

from main.models import SystemLanguage, TokenLanguageString, LanguageString
from main.logger_settings import logger

GLOBAL_LANG = 'ww'

LANGUAGES_DICTIONARY = {
    'ru': {
        'welcome_msg': 'Привет, {}',
        'choose_language_msg': 'Выберите язык',
        'choose_language_btn': 'Русский',
        'error_input_number': 'Номер не верный, введите заново:',
        'error_input_name': 'Имя не должно содержать спец. символов, введите заново:',
        'menu_msg': 'Меню',
        'my_name_btn': 'Мое имя',
        'use_keyboard_msg': 'Используйте виртуальную клавиатуру, пожалуйста!',
        'message_type_not_supported_msg': 'Данный тип сообщений не поддерживается',
        'about_btn': 'О нас',
        'about_msg': 'Мы - лучшие!',
        'project_btn': 'Мои проекты',
        'my_project_msg': 'Бот - @123\n'
                          'Админка - ссылка\n'
                          'Статус "Работает"\n'
                          'Подписка на хостинг истекает: 19.12.21\n'
                          'Домен истекает: 23.02.21',
        'feedback_inline':'Пожаловаться на баг',
        'add_admin_inline':'Добавить админа',
        'back_btn': '⬅️ Назад',
        'ready_btn':'Готово',
        'blocked_msg': 'Вы заблокированы',
        'rename_user': 'Изменить имя',
        'reage_user': 'Изменить возраст',
        'new_name_request_msg': 'Введите свое новое имя:',
        'new_name_error_msg': 'Неправильное имя, попробуйте снова:',
        'cancel': 'Отмена',
        'next': 'Дальше',
        'feedback_msg': 'Вы можете связаться с нами отправив сообщение',
        'template_for_admin_chat_from_user': 'Пришло сообщение от пользователя '
                                             '<a href="tg://user?id={}">{}</a> \n'
                                             'Сообщение:\n'
                                             '{}',
        'feedback_btn': 'Связаться с нами',
        'your_message_to_us_send_done': 'Ваше сообщение отправлено. В ближайшее время вы получите ответ',
        'answer_from_admin_msg': 'Ответ на ваше обращение:\n{}',
        'not_correct_state_for_inline_msg': 'Чтобы воспользоваться этой функцией, '
                                            'вы должны находится в правильном пункте меню',
    }
}


def create_translation(update=False):
    print('===Loading localization===')
    if update:
        print('===Update All strings mode===')
        for lang_code in LANGUAGES_DICTIONARY:
            for token, string in LANGUAGES_DICTIONARY[lang_code].items():
                set_string_for_lang(lang_code, token, string)

    for language in LANGUAGES_DICTIONARY:
        system_language = SystemLanguage.objects.filter(language_code=language).first()
        if not system_language:
            system_language = SystemLanguage(language_code=language)
            system_language.save()
        for token in LANGUAGES_DICTIONARY[language]:
            system_token = TokenLanguageString.objects.filter(name=token).first()
            if not system_token:
                system_token = TokenLanguageString(name=token)
                system_token.save()
            language_string = LanguageString.objects.filter(language_code=system_language,
                                                            token=system_token).first()
            if not language_string:
                new_language_string = LanguageString(language_code=system_language,
                                                     token=system_token,
                                                     string=LANGUAGES_DICTIONARY[language][token])
                new_language_string.save()
            elif language_string.string != LANGUAGES_DICTIONARY[language][token] and update:
                language_string.string = LANGUAGES_DICTIONARY[language][token]
                language_string.save()
    print('===Localization loading finished===')


def add_new_language_default(lang_code):
    system_language_default = SystemLanguage.objects.first()
    system_language = SystemLanguage.objects.filter(language_code=lang_code).first()
    tokens = TokenLanguageString.objects.all()
    for token in tokens:
        string = LanguageString.objects.filter(language_code=system_language_default,
                                               token=token).first()
        LanguageString(language_code=system_language,
                       token=token,
                       string=string.string).save()


def get_translation_for(language, token):
    string_load = LanguageString.objects.filter(language_code__language_code=language,
                                                token__name=token).first()
    if not string_load:
        logger.exception(f"Empty string: {token}")
        return 'Empty string'
    return string_load.string


def generate_and_get_token(lang_code, string):
    name = str(uuid.uuid4().hex)
    token = TokenLanguageString(name=name)
    token.save()
    language = SystemLanguage.objects.filter(language_code=lang_code).first()
    if language is None:
        raise ValueError('Thats language does not exist lang_code: {}'.format(lang_code))
    string = LanguageString(language_code=language, token=token, string=string)
    string.save()
    return token


def set_string_for_lang(lang_code, token_name, string):
    string_lang = LanguageString.objects.filter(language_code__language_code=lang_code, token__name=token_name).first()
    if string_lang:
        string_lang.string = string
    else:
        language = SystemLanguage.objects.filter(language_code=lang_code).first()
        token = TokenLanguageString.objects.filter(name=token_name).first()
        string_lang = LanguageString(language_code=language, token=token, string=string)
    string_lang.save()


def get_languages_list():
    if GLOBAL_LANG in LANGUAGES_DICTIONARY:
        return list(LANGUAGES_DICTIONARY.keys()).remove(GLOBAL_LANG)
    else:
        return list(LANGUAGES_DICTIONARY.keys())
