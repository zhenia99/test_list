import logging
from logging.handlers import RotatingFileHandler

from django_scaffold.settings import LOGGER_MB_LIMIT

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

base_handler = RotatingFileHandler("log/log_info.log", mode='a', maxBytes=LOGGER_MB_LIMIT * 1024 * 1024,
                                   backupCount=1, encoding='utf-8', delay=False)
base_handler.setFormatter(formatter)
base_handler.setLevel(logging.INFO)

logging.basicConfig(level=logging.INFO, handlers=[base_handler])


def setup_logger(name, log_file: str, level=logging.INFO):
    """Function setup as many loggers as you want"""

    handler = RotatingFileHandler(log_file, mode='a', maxBytes=LOGGER_MB_LIMIT*1024*1024, backupCount=1, encoding='utf-8', delay=False)
    handler.setFormatter(formatter)
    handler.setLevel(level)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger


logger = setup_logger('bot', 'log/log_error.log')
