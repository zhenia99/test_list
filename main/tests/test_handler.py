import pytest

from django_scaffold.settings import BOT_TOKEN
from main.bot.handler import BotHandlers
from telebot.types import User as TelegramUser

from main.bot.states import base_state
from main.bot.states.start import StartState
from main.models import User, State

BOT = BotHandlers(BOT_TOKEN)
tg_user = TelegramUser(410371055, False, 'Amateur', 'Leader', 'kotanskiy', 'ru')


class TestState(base_state.BaseState):
    pass


class TestState2(base_state.BaseState):
    pass


@pytest.mark.django_db
def test_get_or_register_user():
    test_user = BOT._BotHandlers__get_or_register_user(tg_user.id, tg_user)
    user = User.objects.filter(user_id_int=tg_user.id).first()
    assert user == test_user


@pytest.mark.django_db
def test_start_handling():
    assert BOT._BotHandlers__start_handling() is None


def test_register_state():
    BOT._BotHandlers__register_state(TestState)
    assert isinstance(BOT._BotHandlers__states[TestState.__name__], TestState)


def test_register_states():
    BOT._BotHandlers__register_states(*[TestState, TestState2])
    assert isinstance(BOT._BotHandlers__states[TestState.__name__], TestState)
    assert isinstance(BOT._BotHandlers__states[TestState2.__name__], TestState2)


def test_get_state():
    BOT._BotHandlers__register_state(TestState)
    test_result_by_string = BOT._BotHandlers__get_state(TestState.__name__)
    assert isinstance(test_result_by_string, tuple)
    assert len(test_result_by_string) == 3
    assert test_result_by_string[0]
    assert isinstance(test_result_by_string[1], TestState)
    assert test_result_by_string[2] == TestState.__name__
    test_result_by_class = BOT._BotHandlers__get_state(TestState)
    assert isinstance(test_result_by_class, tuple)
    assert len(test_result_by_class) == 3
    assert test_result_by_class[0]
    assert isinstance(test_result_by_class[1], TestState)
    assert test_result_by_class[2] == TestState.__name__
    test_result_fail = BOT._BotHandlers__get_state('WrongState')
    assert isinstance(test_result_fail, tuple)
    assert len(test_result_fail) == 3
    assert not test_result_fail[0]
    assert isinstance(test_result_fail[1], StartState)
    assert test_result_fail[2] == StartState.__name__


@pytest.mark.django_db
def test_send_user_to_state():
    user = BOT._BotHandlers__get_or_register_user(tg_user.id, tg_user)
    print(user)
    BOT._BotHandlers__send_user_to_state(None, user, StartState)
    assert State.objects.filter(user=user, name=StartState.__name__).first().name == StartState.__name__









